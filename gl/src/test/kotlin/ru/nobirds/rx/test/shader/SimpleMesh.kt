package ru.nobirds.rx.test.shader

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import ru.nobirds.rx.shader.DrawPrimitive
import ru.nobirds.rx.shader.Shader
import ru.nobirds.rx.shader.mesh.Mesh
import ru.nobirds.rx.shader.mesh.mesh
import ru.nobirds.rx.shader.shader
import ru.nobirds.rx.shader.utils.GlTypes
import ru.nobirds.rx.utils.buffer
import java.nio.ByteBuffer

class SimpleMesh() {

    fun simpleVertex() = Gdx.files.internal("shaders/simple.vertex.glsl").readString()
    fun simpleFragment() = Gdx.files.internal("shaders/simple.fragment.glsl").readString()

    private val shader: Shader = shader {
        vertex(simpleVertex())
        fragment(simpleFragment())

        attributes {
            attribute("a_position")
            attribute("a_color", true, GlTypes.UNSIGNED_BYTE)
        }

        ignoreUnknownUniforms()
    }

    private val buffer: ByteBuffer = buffer(
            // position, color
            -1f, -1f, Color.BLACK.toFloatBits(),
            -1f, 1f, Color.RED.toFloatBits(),
            1f, 1f, Color.GREEN.toFloatBits(),
            1f, -1f, Color.BLUE.toFloatBits()
    )

    private val mesh: Mesh = mesh(shader) {
        withBuffer(buffer)
        withIndex(buffer(0, 1, 2, 3).asShortBuffer())
    }

    fun draw() {
        mesh.draw(DrawPrimitive.triangleFan)
    }

}