/*
package ru.nobirds.rx.test.shader

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.profiling.GLProfiler
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import ru.nobirds.rx.test.application
import ru.nobrds.rx.GenericSceneDirector
import ru.nobrds.rx.shader.texture.AbstractTextureMesh
import ru.nobrds.rx.shader.texture.SingleTextureMesh
import ru.nobrds.rx.utils.Gls
import ru.nobrds.rx.utils.vec3

fun main(args: Array<String>) {
    val application = object : GenericSceneDirector({}) {

        private val texture by lazy { TextureRegion(Texture("whiteflag.png")) }

        private val view by lazy { Matrix4().setToLookAt(vec3(0f, 0f, 10f), vec3(0f, 0f, 0f), Vector3.Y) }

        private val projection = Matrix4()
        private val transform = Matrix4()

        private val textureMesh: AbstractTextureMesh by lazy { SingleTextureMesh(projection) } // by lazy { BatchTextureMesh(projection) }

        override fun create() {
            super.create()
            GLProfiler.enable()
            Gls.enableBlend()
        }

        override fun resize(width: Int, height: Int) {
            projection
                    .ortho(width.toFloat(), height.toFloat())
                    .mul(view)
        }

        override fun render() {
            Gls.clear()
            handleInput()
            textureMesh.projection = projection
            textureMesh.draw(texture, transform)
            textureMesh.flush()
        }

        private fun handleInput() {
            projection.translate(Gdx.input.deltaX.toFloat(), -Gdx.input.deltaY.toFloat(), 0f)
        }
    }

    application(application) {
        vSyncEnabled = true
        fullscreen = false
        useGL30 = true
    }
}

fun Matrix4.ortho(width:Float, height:Float, near:Float = 1f, far:Float = 100f):Matrix4 {
    return setToOrtho(-width/2f, width/2f, -height/2f, height/2f, near, far)
}*/
