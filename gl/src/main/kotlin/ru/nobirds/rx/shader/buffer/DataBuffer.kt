package ru.nobirds.rx.shader.buffer

import com.badlogic.gdx.utils.Disposable
import ru.nobirds.rx.shader.Bindable
import java.nio.Buffer

interface DataBuffer<B: Buffer> : Disposable, Bindable {

    val size:Int

    val sizeInBytes:Int
        get() = size * bufferElementSize

    val bufferElementSize:Int

    fun set(buffer: B)

}