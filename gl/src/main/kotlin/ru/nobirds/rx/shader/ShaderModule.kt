package ru.nobirds.rx.shader

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils
import com.badlogic.gdx.utils.Disposable
import ru.nobirds.rx.shader.utils.Gl

class ShaderModule(val type: ShaderType, val source:String) : Disposable {

    private var handle:Int? = null

    fun compile(): Int {
        if (isValidShader(handle)) {
            return handle!!
        }

        this.handle = createShader(source)

        return compileShader(this.handle!!, source)
    }

    private fun isValidShader(handle: Int?): Boolean {
        if(handle == null)
            return false

        return Gl.glIsShader(handle)
    }

    private fun compileShader(shader: Int, source: String): Int {
        val result = BufferUtils.newIntBuffer(1)

        Gl.glShaderSource(shader, source)
        Gl.glCompileShader(shader)
        Gl.glGetShaderiv(shader, GL20.GL_COMPILE_STATUS, result)

        val compiled = result.get(0)

        if (compiled == GL20.GL_FALSE) {
            val errorMessage = Gl.glGetShaderInfoLog(shader)

            throw ShaderCompileException(source, errorMessage)
        }

        return shader
    }

    private fun createShader(source: String): Int {
        val shader = Gl.glCreateShader(type.glType)

        if (shader == GL20.GL_FALSE)
            throw ShaderCompileException(source, "Can't create shader of type $type")

        return shader
    }

    override fun dispose() {
        if (isValidShader(this.handle)) {
            Gl.glDeleteShader(this.handle!!)
            this.handle = null
        }
    }
}