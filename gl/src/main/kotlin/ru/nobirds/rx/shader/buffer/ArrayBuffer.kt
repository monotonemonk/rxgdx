package ru.nobirds.rx.shader.buffer

import ru.nobirds.rx.shader.DrawMode
import java.nio.ByteBuffer

class ArrayBuffer(buffer: ByteBuffer, drawMode: DrawMode) :
        DataBufferImpl<ByteBuffer>(BufferType.Array, buffer, drawMode, 1)