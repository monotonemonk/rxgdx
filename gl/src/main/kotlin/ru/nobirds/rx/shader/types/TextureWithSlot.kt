package ru.nobirds.rx.shader.types

data class TextureWithSlot(val texture: Int, val slot:Int)
