package ru.nobirds.rx.shader.mesh

import ru.nobirds.rx.shader.BufferDrawer
import ru.nobirds.rx.shader.DrawPrimitive
import ru.nobirds.rx.shader.Shader
import ru.nobirds.rx.shader.bind
import java.util.HashMap

class Mesh(val shader: Shader, val drawer: BufferDrawer,
           values:Map<String, Any> = emptyMap()) {

    private val values = HashMap<String, Any>(values)

    operator fun set(name:String, value:Any) {
        values[name] = value
    }

    fun remove(name:String) {
        values.remove(name)
    }

    operator fun get(name: String): Any? = values[name]

    fun draw(primitive: DrawPrimitive) {
        shader.bind {
            set(values)
            drawer.draw(primitive)
        }
    }

}

fun mesh(shader: Shader, builder: MeshBuilder.()->Unit): Mesh =
        MeshBuilder(shader).apply { builder() }.build()

