package ru.nobirds.rx.shader.types

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.shader.UniformType
import ru.nobirds.rx.shader.utils.Gl
import ru.nobirds.rx.utils.instanceOf
import ru.nobirds.rx.shader.utils.throwOnError
import kotlin.reflect.KClass

class PrimitiveUniformType<T:Any>(override val type: Int, override val size: Int,
                                  val  primitiveType: KClass<T>, val setter: GL20.(Int, T)->Unit) : UniformType {

    override fun set(location: Int, value: Any) =
            Gl.throwOnError { setter(location, value as T) }

    // todo: check instance right
    private fun check(value: Any) {
            if (!(value instanceOf primitiveType))
                throw IllegalArgumentException(
                    "Unsupported type of parameter: required $primitiveType, but passed ${value.javaClass}")
    }
}