package ru.nobirds.rx.shader.types

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.shader.UniformType
import ru.nobirds.rx.shader.utils.Gl
import ru.nobirds.rx.shader.utils.throwOnError
import java.nio.FloatBuffer

fun floatVectorUniform(type: Int, size: Int, count:Int,
                       setArray: GL20.(Int, Int, FloatArray, Int)->Unit,
                       setBuffer: GL20.(Int, Int, FloatBuffer)->Unit): UniformType =
        FloatVectorUniformType(type, size, count, setArray, setBuffer)

class FloatVectorUniformType (
        override val type: Int,
        override val size: Int,
        val count:Int,
        val setArray: GL20.(Int, Int, FloatArray, Int)->Unit,
        val setBuffer: GL20.(Int, Int, FloatBuffer)->Unit) : UniformType {

    override fun set(location: Int, value: Any) {
        when (value) {
            is FloatArray -> Gl.throwOnError { setArray(location, size, validate(value, value.size), 0) }
            is FloatBuffer -> Gl.throwOnError { setBuffer(location, size, validate(value, value.limit())) }
            else -> throw IllegalArgumentException(
                    "Unsupported type of parameter: required float4 compatible, but passed ${value.javaClass}")
        }
    }

    private fun <T> validate(value: T, size:Int): T {
        if(size != count)
            throw IllegalArgumentException("Required size $count, but passed $size")

        return value
    }

}