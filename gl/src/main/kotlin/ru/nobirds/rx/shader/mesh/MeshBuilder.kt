package ru.nobirds.rx.shader.mesh

import ru.nobirds.rx.shader.ArrayBufferDrawer
import ru.nobirds.rx.shader.DrawMode
import ru.nobirds.rx.shader.ElementsArrayBufferDrawer
import ru.nobirds.rx.shader.Shader
import ru.nobirds.rx.shader.buffer.ArrayBuffer
import ru.nobirds.rx.shader.buffer.ElementsArrayBuffer
import ru.nobirds.rx.shader.buffer.IntElementsArrayBuffer
import ru.nobirds.rx.shader.buffer.ShortElementsArrayBuffer
import ru.nobirds.rx.shader.buffer.StructuredArrayBuffer
import ru.nobirds.rx.shader.buffer.structuredArrayBuffer
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.ShortBuffer
import java.util.HashMap

class MeshBuilder(val shader: Shader, val mode: DrawMode = DrawMode.static) {

    private var data: StructuredArrayBuffer? = null

    private var index: ElementsArrayBuffer<*>? = null

    private val values = HashMap<String, Any>()

    fun withBuffer(buffer: ByteBuffer): MeshBuilder =
            withBuffer(ArrayBuffer(buffer, mode))

    fun withBuffer(buffer: ArrayBuffer): MeshBuilder = apply {
        this.data = structuredArrayBuffer(shader, buffer)
    }

    fun withIndex(index: ShortBuffer): MeshBuilder = apply {
        this.index = ShortElementsArrayBuffer(index, mode)
    }

    fun withIndex(index: IntBuffer): MeshBuilder = apply {
        this.index = IntElementsArrayBuffer(index, mode)
    }

    fun withIndex(index: ElementsArrayBuffer<*>): MeshBuilder = apply {
        this.index = index
    }

    fun withValue(name: String, value:Any): MeshBuilder = apply {
        values.put(name, value)
    }

    fun build(): Mesh {
        val data = data ?: throw IllegalArgumentException("Buffer must be defined")
        val index = index

        val drawer = if (index != null)
            ElementsArrayBufferDrawer(data, index)
        else
            ArrayBufferDrawer(data)

        return Mesh(shader, drawer, values)
    }
}