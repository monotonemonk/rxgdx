package ru.nobirds.rx.shader

import ru.nobirds.rx.shader.buffer.ElementsArrayBuffer
import ru.nobirds.rx.shader.buffer.StructuredArrayBuffer
import ru.nobirds.rx.shader.utils.Gl

interface BufferDrawer {

    fun draw(primitive: DrawPrimitive)

}

abstract class AbstractArrayBufferDrawer(val data: StructuredArrayBuffer) : BufferDrawer {

    override fun draw(primitive: DrawPrimitive) {
        data.bind {
            drawImpl(primitive)
        }
    }

    abstract fun drawImpl(primitive: DrawPrimitive)

}

class ArrayBufferDrawer(data: StructuredArrayBuffer) : AbstractArrayBufferDrawer(data) {

    override fun drawImpl(primitive: DrawPrimitive) {
        Gl.glDrawArrays(primitive.gl, 0, data.count)
    }
}

class ElementsArrayBufferDrawer(data: StructuredArrayBuffer, val index: ElementsArrayBuffer<*>) :
        AbstractArrayBufferDrawer(data) {

    override fun drawImpl(primitive: DrawPrimitive) {
        index.bind {
            Gl.glDrawElements(primitive.gl, index.size, index.glType, 0)
        }
    }

}