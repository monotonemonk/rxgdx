package ru.nobirds.rx.shader.utils

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils
import ru.nobirds.rx.shader.utils.Gl
import java.util.HashMap
import java.util.HashSet

interface TextureUnits {

    fun bind(texture:Int):Int

    fun bind(texture:Int, slot: Int):Int

    fun clean()

}

class SimpleTextureUnits() : TextureUnits {

    private val MAX_GL_ES_UNITS = 32

    private val cache = HashMap<Int, Int>()
    private val slots = HashSet<Int>()

    private val capacity = Math.min(glMaxCapacity(), MAX_GL_ES_UNITS)

    override fun bind(texture: Int): Int {
        return bind(texture, getCachedOrFindFreeSlot(texture))
    }

    private fun getCachedOrFindFreeSlot(texture: Int): Int = cache[texture] ?: findFreeSlot()

    private fun findFreeSlot(): Int = (1..capacity-1).first { it !in slots }

    override fun bind(texture: Int, slot: Int): Int {
        glBind(texture, slot)

        slots.add(slot)
        cache[texture] = slot

        return slot
    }

    private fun glBind(texture: Int, slot: Int) {
        Gl.glActiveTexture(GL20.GL_TEXTURE0 + slot)
        Gl.glBindTexture(GL20.GL_TEXTURE_2D, texture)
        // Gl.glActiveTexture(GL20.GL_TEXTURE0)
    }

    private fun glMaxCapacity(): Int {
        val buffer = BufferUtils.newIntBuffer(16)
        Gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_IMAGE_UNITS, buffer)
        return buffer.get(0)
    }


    override fun clean() {
        cache.clear()
        slots.clear()
    }

}

object Textures {

    val units: TextureUnits = SimpleTextureUnits()

}