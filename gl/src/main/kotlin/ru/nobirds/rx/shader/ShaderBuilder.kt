package ru.nobirds.rx.shader

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.BufferUtils
import ru.nobirds.rx.shader.types.AttributeTypeOptions
import ru.nobirds.rx.shader.types.AttributeTypes
import ru.nobirds.rx.shader.types.UniformTypes
import ru.nobirds.rx.shader.utils.GlType
import ru.nobirds.rx.shader.utils.Gl
import ru.nobirds.rx.utils.files.ImmutableResource
import ru.nobirds.rx.utils.files.readText
import java.nio.Buffer
import java.util.HashMap
import java.util.LinkedHashMap

class ShaderModuleBuilder(val type:ShaderType) {

    private var text:String? = null

    fun text(text: String):ShaderModuleBuilder = apply {
        this.text = text
    }

    fun text(resource: ImmutableResource):ShaderModuleBuilder = apply {
        text(resource.readText())
    }

    fun build():ShaderModule {
        val shaderText = text ?: throw IllegalStateException("Shader text must be defined")
        return ShaderModule(type, shaderText)
    }

}

interface VertexStructureBuilder {

    fun attribute(name: String, normalized:Boolean = false, glType: GlType? = null)

}

class ShaderBuilder : VertexStructureBuilder {

    private val resultBuffer = BufferUtils.newIntBuffer(1)
    private val typeBuffer = BufferUtils.newIntBuffer(1)

    private val conveyor = HashMap<ShaderType, ShaderModule>()

    private var ignoreUnknownUniforms = false

    private val attributes = LinkedHashMap<String, AttributeTypeOptions>()

    fun ignoreUnknownUniforms(value:Boolean = true):ShaderBuilder = apply {
        this.ignoreUnknownUniforms = value
    }

    fun vertex(text:String): ShaderBuilder = vertex { text(text) }
    fun fragment(text:String): ShaderBuilder = fragment { text(text) }
    
    fun module(module: ShaderModule): ShaderBuilder = apply {
        conveyor.put(module.type, module)
    }
    
    inline fun vertex(builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(ShaderType.vertex, builder)
    inline fun fragment(builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(ShaderType.fragment, builder)

    inline fun module(type: ShaderType, builder:ShaderModuleBuilder.()->Unit): ShaderBuilder = module(
            ShaderModuleBuilder(type).apply(builder).build()
    )

    override fun attribute(name: String, normalized:Boolean, glType: GlType?) {
        attributes.put(name, AttributeTypeOptions(normalized, glType))
    }

    fun attributes(builder:VertexStructureBuilder.()->Unit) {
        this.builder()
    }

    fun build(): Shader {
        val shaders = conveyor.values
                .sortedBy { it.type.ordinal }
                .map { it.compile() }

        val program = createProgram()

        attachShaders(program, shaders)

        linkProgram(program)

        detachAndDeleteShaders(program, shaders)

        val uniforms = findUniforms(program)
        val attributes = findAttributes(program).associateBy { it.name }

        val sortedAttributes = sortAttributes(attributes)

        return ShaderImpl(program, uniforms, sortedAttributes, ignoreUnknownUniforms)
    }

    private fun sortAttributes(attributes: Map<String, Attribute>): List<Attribute> {
        return if (this.attributes.isNotEmpty()) this.attributes.keys.map {
            attributes[it] ?: throw IllegalStateException("Attribute with name $it not found in shader")
        } else attributes.values.sortedBy { it.location }
    }

    private fun detachAndDeleteShaders(program: Int, shaders: List<Int>) {
        for (shader in shaders) {
            Gl.glDetachShader(program, shader)
            Gl.glDeleteShader(shader)
        }
    }

    private fun attachShaders(program: Int, shaders: List<Int>) {
        for (shader in shaders) {
            Gl.glAttachShader(program, shader)
        }
    }

    private fun findUniforms(program: Int): List<Uniform> {
        val count = getProgramAttribute(program, GL20.GL_ACTIVE_UNIFORMS)
        return (0..count-1).map { findUniform(program, it) }
    }

    private fun findUniform(program: Int, index: Int): Uniform {
        val name = Gl.glGetActiveUniform(program, index, resultBuffer.renew(), typeBuffer.renew())
        val type = typeBuffer.get(0)
        val size = resultBuffer.get(0)
        val location = Gl.glGetUniformLocation(program, name)

        val realName = name.substringBefore("[")

        return UniformImpl(realName, UniformTypes.get(type, size), location)
    }

    private fun findAttributes(program: Int): List<Attribute> {
        val count = getProgramAttribute(program, GL20.GL_ACTIVE_ATTRIBUTES)
        return (0..count-1).map { findAttribute(program, it) }
    }

    private fun findAttribute(program: Int, index: Int): Attribute {
        val name = Gl.glGetActiveAttrib(program, index, resultBuffer.renew(), typeBuffer.renew())
        val type = typeBuffer.get(0)
        val size = resultBuffer.get(0)
        val location = Gl.glGetAttribLocation(program, name)

        val options = attributes[name] ?:
                (if(attributes.isEmpty()) AttributeTypeOptions() else
                    throw IllegalStateException("Options for attribute $name not defined"))

        return AttributeImpl(name, AttributeTypes.get(type, size, options), location)
    }

    private fun linkProgram(program: Int) {
        Gl.glLinkProgram(program)

        val linked = getProgramAttribute(program, GL20.GL_LINK_STATUS)

        if (linked == GL20.GL_FALSE) {
            val log = Gl.glGetProgramInfoLog(program)
            throw ShaderException("Can't link shader program: $log")
        }
    }

    private fun createProgram(): Int {
        val program = Gl.glCreateProgram()
        if (program == GL20.GL_FALSE)
            throw ShaderException("Can't create shader program")
        return program
    }

    private fun getProgramAttribute(program: Int, attribute:Int): Int {
        Gl.glGetProgramiv(program, attribute, resultBuffer.renew())
        return resultBuffer.get(0)
    }

    private fun <B: Buffer> B.renew():B = this.clear() as B
}

fun shader(builder: ShaderBuilder.()->Unit): Shader {
    val shaderBuilder = ShaderBuilder()
    shaderBuilder.builder()
    return shaderBuilder.build()
}
