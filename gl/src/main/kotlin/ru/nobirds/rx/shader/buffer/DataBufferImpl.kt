package ru.nobirds.rx.shader.buffer

import ru.nobirds.rx.shader.DrawMode
import ru.nobirds.rx.shader.bind
import ru.nobirds.rx.shader.utils.Gl
import java.nio.Buffer

open class DataBufferImpl<B: Buffer>(val bufferType: BufferType, buffer: B, val drawMode: DrawMode, override val bufferElementSize:Int) : DataBuffer<B> {

    private val handle = Gl.glGenBuffer()

    private var sizeField:Int = 0
    override val size:Int
        get() = sizeField

    init {
        set(buffer)
    }

    override fun set(buffer: B) {
        bind {
            this.sizeField = buffer.limit()
            Gl.glBufferData(bufferType.gl, size, buffer, drawMode.gl)
        }
    }

    override fun bind() {
        Gl.glBindBuffer(bufferType.gl, handle)
    }

    override fun unbind() {
        Gl.glBindBuffer(bufferType.gl, 0)
    }

    override fun dispose() {
        Gl.glDeleteBuffer(handle)
    }

}