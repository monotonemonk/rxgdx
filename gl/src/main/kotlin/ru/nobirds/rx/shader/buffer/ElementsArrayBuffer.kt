package ru.nobirds.rx.shader.buffer

import com.badlogic.gdx.graphics.GL20
import ru.nobirds.rx.shader.DrawMode
import java.nio.Buffer
import java.nio.IntBuffer
import java.nio.ShortBuffer

open class ElementsArrayBuffer<B: Buffer>(buffer: B, drawMode: DrawMode, size:Int, val glType:Int) :
        DataBufferImpl<B>(BufferType.ElementArray, buffer, drawMode, size)

class ShortElementsArrayBuffer(buffer: ShortBuffer, drawMode: DrawMode) : ElementsArrayBuffer<ShortBuffer>(buffer, drawMode, 2, GL20.GL_UNSIGNED_SHORT)
class IntElementsArrayBuffer(buffer: IntBuffer, drawMode: DrawMode) : ElementsArrayBuffer<IntBuffer>(buffer, drawMode, 4, GL20.GL_UNSIGNED_INT)

