package ru.nobirds.rx.shader.buffer

import com.badlogic.gdx.utils.Disposable
import ru.nobirds.rx.shader.Bindable
import ru.nobirds.rx.shader.NeedsGl3Support
import ru.nobirds.rx.shader.Shader
import ru.nobirds.rx.shader.bind
import ru.nobirds.rx.shader.then
import ru.nobirds.rx.shader.utils.glDeleteVertexArray
import ru.nobirds.rx.shader.utils.glGenVertexArray
import ru.nobirds.rx.shader.utils.Gl3
import ru.nobirds.rx.shader.utils.Gls

fun structuredArrayBuffer(shader: Shader, buffer:ArrayBuffer):StructuredArrayBuffer =
        if(Gls.gl3Supported)
            VertexArray(shader, buffer)
        else
            StructuredVertexBuffer(shader, buffer)

interface StructuredArrayBuffer : Disposable, Bindable {

    val vertexSize:Int

    val count:Int

    val buffer:ArrayBuffer

}

abstract class AbstractStructuredArrayBuffer(shader: Shader, override val buffer:ArrayBuffer) : StructuredArrayBuffer {

    override val vertexSize:Int = shader.vertexSize

    override val count: Int
        get() = buffer.sizeInBytes/ vertexSize

}

class StructuredVertexBuffer(val shader: Shader, buffer:ArrayBuffer) : AbstractStructuredArrayBuffer(shader, buffer) {

    override fun bind() {
        buffer.bind()
        shader.bindAttributes()
    }

    override fun unbind() {
        buffer.unbind()
    }

    override fun dispose() {
        buffer.dispose()
    }

}

@NeedsGl3Support
class VertexArray(shader: Shader, buffer:ArrayBuffer) : AbstractStructuredArrayBuffer(shader, buffer) {

    private val handle = Gl3.glGenVertexArray()

    init {
        (buffer then this).bind {
            shader.bindAttributes()
        }
    }

    override fun bind() {
        Gl3.glBindVertexArray(handle)
    }

    override fun unbind() {
        Gl3.glBindVertexArray(0)
    }

    override fun dispose() {
        Gl3.glDeleteVertexArray(handle)
        buffer.dispose()
    }

}

