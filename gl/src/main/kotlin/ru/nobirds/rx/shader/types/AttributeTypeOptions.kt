package ru.nobirds.rx.shader.types

import ru.nobirds.rx.shader.utils.GlType

data class AttributeTypeOptions(val normalized:Boolean = false, val glType: GlType? = null)