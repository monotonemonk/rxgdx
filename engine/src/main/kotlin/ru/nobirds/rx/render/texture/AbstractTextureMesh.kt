package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.BufferUtils
import ru.nobirds.rx.shader.DrawMode
import ru.nobirds.rx.shader.DrawPrimitive
import ru.nobirds.rx.shader.buffer.ArrayBuffer
import ru.nobirds.rx.shader.buffer.ElementsArrayBuffer
import ru.nobirds.rx.shader.buffer.ShortElementsArrayBuffer
import ru.nobirds.rx.shader.mesh.Mesh
import ru.nobirds.rx.shader.mesh.property
import ru.nobirds.rx.utils.array
import java.nio.ByteBuffer
import java.nio.ShortBuffer

abstract class AbstractTextureMesh(projection: Matrix4, val maxRecordsCount:Int = 5000) : TextureMesh {

    private val vertexCount = 4
    private val quadVertexCount = 6

    protected abstract val recordSize:Int

    protected var recordsCount = 0

    private val buffer = BufferUtils.newByteBuffer(maxRecordsCount * recordSize)

    private val index = BufferUtils.newShortBuffer(maxRecordsCount * quadVertexCount)

    private val deltas = arrayOf(0, 1, 2, 2, 3, 0)

    private fun ShortBuffer.putIndex(index:Int) {
        for (delta in deltas) {
            put((index+delta).toShort())
        }
    }

    protected val arrayBuffer: ArrayBuffer = ArrayBuffer(buffer, DrawMode.dynamic)
    protected val elementsBuffer:ElementsArrayBuffer<ShortBuffer> = ShortElementsArrayBuffer(index, DrawMode.dynamic)

    protected val mesh: Mesh = createMesh()

    protected abstract fun createMesh(): Mesh

    var projection: Matrix4 by mesh.property("u_projection", projection) { array }

    override fun draw(texture: Texture,
                      x1:Float, y1:Float,
                      x2:Float, y2:Float,
                      x3:Float, y3:Float,
                      x4:Float, y4:Float,
                      u1:Float, v1:Float,
                      u2:Float, v2:Float,
                      tint: Color) {

        if (isBatchFull(texture))
            flush()

        write(texture,
                buffer,
                x1, y1,
                x2, y2,
                x3, y3,
                x4, y4,
                u1, v1,
                u2, v2,
                tint.toFloatBits())

        index.putIndex(recordsCount * vertexCount)

        recordsCount++
    }

    private fun clean() {
        buffer.clear()
        index.clear()

        recordsCount = 0

        cleanImpl()
    }

    override fun flush() {
        if (recordsCount > 0) {
            buffer.flip()
            arrayBuffer.set(buffer)

            index.flip()
            elementsBuffer.set(index)

            setUniforms(mesh)

            mesh.draw(DrawPrimitive.triangles)
        }
        clean()
    }

    protected abstract fun cleanImpl()

    protected abstract fun write(texture: Texture,
                                 buffer: ByteBuffer,
                                 x1:Float, y1:Float,
                                 x2:Float, y2:Float,
                                 x3:Float, y3:Float,
                                 x4:Float, y4:Float,
                                 u1:Float, v1:Float,
                                 u2:Float, v2:Float,
                                 color:Float)

    protected abstract fun isBatchFull(texture: Texture):Boolean

    protected open fun setUniforms(mesh: Mesh) {
    }

}


