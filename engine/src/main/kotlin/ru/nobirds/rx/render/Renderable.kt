package ru.nobirds.rx.render

import ru.nobirds.rx.Projection

interface Renderable {

    fun render(projection: Projection)

}