package ru.nobirds.rx.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Affine2
import ru.nobirds.rx.Projection
import ru.nobirds.rx.utils.ImmutableBounds
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.ImmutableTransform
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.x1
import ru.nobirds.rx.utils.x2
import ru.nobirds.rx.utils.x3
import ru.nobirds.rx.utils.x4
import ru.nobirds.rx.utils.y1
import ru.nobirds.rx.utils.y2
import ru.nobirds.rx.utils.y3
import ru.nobirds.rx.utils.y4

interface RenderConveyor {

    fun begin()

    fun end()

    var projection: Projection

    fun render(renderable: Renderable)

    fun render(texture: Texture,
               x1:Float, y1:Float,
               x2:Float, y2:Float,
               x3:Float, y3:Float,
               x4:Float, y4:Float,
               u1:Float, v1:Float,
               u2:Float, v2:Float,
               tint: Color = Color.WHITE)

    fun renderRect(x1:Float, y1:Float,
                   x2:Float, y2:Float,
                   x3:Float, y3:Float,
                   x4:Float, y4:Float, color: Color)

}

fun RenderConveyor.render(texture: Texture,
                          x1: Float, y1: Float,
                          x2: Float, y2: Float,
                          u1: Float, v1: Float,
                          u2: Float, v2: Float,
                          tint: Color = Color.WHITE) {

    render(texture,
            x1, y1,
            x1, y2,
            x2, y2,
            x2, y1,
            u1, v1,
            u2, v2,
            tint)

}

fun RenderConveyor.render(region: TextureRegion, transform: ImmutableTransform) {
    val affine = transform.toAffine()

    val width = region.regionWidth.toFloat()
    val height = region.regionHeight.toFloat()

    render(region, affine, width, height)
}

fun RenderConveyor.render(region: TextureRegion, transform: ImmutableTransform, bounds: ImmutableBounds) = poolable {
    val tmp = obtain<Affine2>().set(transform.toAffine())

    tmp.translate(bounds.x, bounds.y)

    render(region, tmp, bounds.width, bounds.height)
}

private fun RenderConveyor.render(region: TextureRegion, affine: Affine2, width: Float, height: Float) {

    val x1 = affine.x1()
    val y1 = affine.y1()
    val x2 = affine.x2(height)
    val y2 = affine.y2(height)
    val x3 = affine.x3(width, height)
    val y3 = affine.y3(width, height)
    val x4 = affine.x4(width)
    val y4 = affine.y4(width)

    render(region.texture, x1, y1, x2, y2, x3, y3, x4, y4,
            region.u, region.v2, region.u2, region.v)
}

fun RenderConveyor.render(font: BitmapFont, layout: GlyphLayout, transform: ImmutableTransform) = poolable {
    val affine = obtain<Affine2>()
    val subAffine = obtain<Affine2>()

    for (run in layout.runs) {
        val advances = run.xAdvances

        affine.set(transform.toAffine()).translate(run.x, run.y)

        var dx = 0f
        run.glyphs.forEachIndexed { i, glyph ->
            dx += advances[i]
            renderGlyph(font, glyph, run.color, subAffine.set(affine), dx, font.lineHeight)
        }
    }
}

private fun RenderConveyor.renderGlyph(font: BitmapFont, glyph: BitmapFont.Glyph,
                                       color: Color, affine: Affine2, dx:Float, dy:Float) {
    val scaleX = font.data.scaleX
    val scaleY = font.data.scaleY

    affine.translate(
            dx + (glyph.xoffset.toFloat() * scaleX),
            dy + (glyph.yoffset.toFloat() * scaleY)
    )

    var width = glyph.width.toFloat() * scaleX
    var height = glyph.height.toFloat() * scaleY

    val texture = font.getRegion(glyph.page).texture

    val x1 = affine.x1()
    val y1 = affine.y1()
    val x2 = affine.x2(height)
    val y2 = affine.y2(height)
    val x3 = affine.x3(width, height)
    val y3 = affine.y3(width, height)
    val x4 = affine.x4(width)
    val y4 = affine.y4(width)

    render(texture,
            x1, y1,
            x2, y2,
            x3, y3,
            x4, y4,
            glyph.u, glyph.v,
            glyph.u2, glyph.v2,
            color)
}

fun RenderConveyor.renderRect(transform: ImmutableTransform, size: ImmutableSize, color: Color) = poolable {
    val affine = transform.toAffine()

    val width = size.width
    val height = size.height

    val x1 = affine.x1()
    val y1 = affine.y1()
    val x2 = affine.x2(height)
    val y2 = affine.y2(height)
    val x3 = affine.x3(width, height)
    val y3 = affine.y3(width, height)
    val x4 = affine.x4(width)
    val y4 = affine.y4(width)

    renderRect(x1, y1, x2, y2, x3, y3, x4, y4, color)
}

inline fun RenderConveyor.render(projection: Projection, block: RenderConveyor.()->Unit) {
    if(this.projection != projection)
        this.projection = projection

    begin()
    block()
    end()
}

