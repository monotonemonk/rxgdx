package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.render.PredefinedShaders
import ru.nobirds.rx.shader.NeedsGl3Support
import ru.nobirds.rx.shader.mesh.Mesh
import ru.nobirds.rx.shader.mesh.mesh
import ru.nobirds.rx.utils.put
import java.nio.ByteBuffer
import java.util.HashMap

internal class TextureArray(val size: Int) {

    private val index = HashMap<Int, Int>()
    private val array = IntArray(size)

    private var nextIndex = 0

    val full:Boolean
        get() = nextIndex + 1 >= size

    fun position(texture: Int):Int = index.getOrPut(texture) { add(texture) }

    private fun add(texture: Int): Int {
        assert(!full)

        val idx = nextIndex++

        for (i in idx..array.size - 1) {
            array[i] = texture
        }

        return idx
    }

    fun clear() {
        nextIndex = 0
        index.clear()
    }

    fun get():IntArray = array

    operator fun contains(texture: Int):Boolean = texture in index

}

@NeedsGl3Support
class BatchTextureMesh(projection: Matrix4) : AbstractTextureMesh(projection) {

    override val recordSize: Int
        get() = 4 * 9

    private val textures = TextureArray(10)

    override fun createMesh(): Mesh = mesh(PredefinedShaders.textureBatch) {
        withBuffer(arrayBuffer)
        withIndex(elementsBuffer)
    }

    override fun cleanImpl() {
        textures.clear()
    }

    override fun write(texture: Texture,
                       buffer: ByteBuffer,
                       x1: Float, y1: Float,
                       x2: Float, y2: Float,
                       x3: Float, y3: Float,
                       x4: Float, y4: Float,
                       u1: Float, v1: Float,
                       u2: Float, v2: Float,
                       color:Float) {

        val index = textures.position(texture.textureObjectHandle).toFloat()

        buffer.put(
                index, x1, y1, u1, v1, color,
                index, x2, y2, u1, v2, color,
                index, x3, y3, u2, v2, color,
                index, x4, y4, u2, v1, color)
    }

    override fun isBatchFull(texture: Texture): Boolean =
            recordsCount == maxRecordsCount || (texture.textureObjectHandle !in textures && textures.full)


    override fun setUniforms(mesh: Mesh) {
        mesh["u_textures"] = textures.get()
    }

}