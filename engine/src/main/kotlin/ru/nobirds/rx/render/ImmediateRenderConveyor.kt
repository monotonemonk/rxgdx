package ru.nobirds.rx.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.Projection
import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.render.texture.BatchTextureMesh
import ru.nobirds.rx.render.texture.RenderMesh
import ru.nobirds.rx.render.texture.SingleTextureMesh
import ru.nobirds.rx.shader.utils.Gls
import ru.nobirds.rx.utils.ifChanged
import kotlin.properties.Delegates

class ImmediateRenderConveyor(val blend:Boolean = true) : RenderConveyor {

    private var subscription:Subscription? = null

    override var projection: Projection by Delegates.vetoable<Projection>(NoneProjection) { p, o, n ->
        ifChanged(o, n) {
            textureMesh.projection = n.matrix
            shapeMesh.projection = n.matrix

            if(subscription != null)
                subscription?.unsubscribe()

            subscription = n.invalidated.subscribe {
                textureMesh.projection = n.matrix
                shapeMesh.projection = n.matrix
            }
        }
    }

    private val textureMesh = if(Gls.gl3Supported) BatchTextureMesh(projection.matrix) else SingleTextureMesh(projection.matrix)

    private val shapeMesh = ShapesMesh(projection.matrix)

    private var renderMesh: RenderMesh? by Delegates.observable<RenderMesh?>(null) { p, o, n ->
        if (o !== n) {
            o?.flush()
        }
    }

    override fun begin() {
        if(blend)
            Gls.enableBlend()
    }

    override fun end() {
        renderMesh?.flush()
    }

    override fun render(renderable: Renderable) {
        renderMesh = null
        renderable.render(projection)
    }

    override fun render(texture: Texture,
                        x1:Float, y1:Float,
                        x2:Float, y2:Float,
                        x3:Float, y3:Float,
                        x4:Float, y4:Float,
                        u1:Float, v1:Float,
                        u2:Float, v2:Float,
                        tint:Color) {

        renderMesh = textureMesh

        textureMesh.draw(texture,
                x1, y1,
                x2, y2,
                x3, y3,
                x4, y4,
                u1, v1,
                u2, v2,
                tint)
    }

    override fun renderRect(x1:Float, y1:Float,
                            x2:Float, y2:Float,
                            x3:Float, y3:Float,
                            x4:Float, y4:Float, color:Color) {
        renderMesh = shapeMesh
        shapeMesh.drawRect(x1, y1, x2, y2, x3, y3, x4, y4, color)
    }

}