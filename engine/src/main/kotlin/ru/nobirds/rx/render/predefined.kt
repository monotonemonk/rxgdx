package ru.nobirds.rx.render

import ru.nobirds.rx.Director
import ru.nobirds.rx.shader.Shader
import ru.nobirds.rx.shader.shader
import ru.nobirds.rx.shader.utils.GlTypes
import ru.nobirds.rx.utils.files.readText

object PredefinedShaders {

    val texture: Shader by lazy {
        shader {
            vertex(Director.resources.forRead("shaders/texture.vertex.glsl").readText())
            fragment(Director.resources.forRead("shaders/texture.fragment.glsl").readText())

            attributes {
                attribute("a_position")
                attribute("a_texture_position")
                attribute("a_tint_color", true, GlTypes.UNSIGNED_BYTE)
            }
        }
    }

    val textureBatch: Shader by lazy {
        shader {
            vertex(Director.resources.forRead("shaders/texture.batch.vertex.glsl").readText())
            fragment(Director.resources.forRead("shaders/texture.batch.fragment.glsl").readText())

            attributes {
                attribute("a_texture_number")
                attribute("a_position")
                attribute("a_texture_position")
                attribute("a_tint_color", true, GlTypes.UNSIGNED_BYTE)
            }
        }
    }

}