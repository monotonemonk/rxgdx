package ru.nobirds.rx.render.texture

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.render.PredefinedShaders
import ru.nobirds.rx.shader.mesh.Mesh
import ru.nobirds.rx.shader.mesh.mesh
import ru.nobirds.rx.shader.mesh.property
import ru.nobirds.rx.utils.put
import java.nio.ByteBuffer

class SingleTextureMesh(projection: Matrix4) : AbstractTextureMesh(projection) {

    private var texture: Texture? by mesh.property<Texture?>("u_texture", null) { this?.textureObjectHandle }

    override val recordSize: Int
        get() = 4 * 8

    override fun createMesh(): Mesh = mesh(PredefinedShaders.texture) {
        withBuffer(arrayBuffer)
        withIndex(elementsBuffer)
    }

    override fun write(texture: Texture,
                       buffer: ByteBuffer,
                       x1:Float, y1:Float,
                       x2:Float, y2:Float,
                       x3:Float, y3:Float,
                       x4:Float, y4:Float,
                       u1:Float, v1:Float,
                       u2:Float, v2:Float,
                       color:Float) {

        buffer.put(
                x1, y1, u1, v1, color,
                x2, y2, u1, v2, color,
                x3, y3, u2, v2, color,
                x4, y4, u2, v1, color)

        this.texture = texture
    }

    override fun isBatchFull(texture: Texture): Boolean =
            recordsCount == maxRecordsCount || texture != this.texture

    override fun cleanImpl() {
        this.texture = null
    }

}