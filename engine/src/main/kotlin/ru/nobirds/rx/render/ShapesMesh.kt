package ru.nobirds.rx.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import ru.nobirds.rx.render.texture.RenderMesh
import kotlin.properties.Delegates

class ShapesMesh(projection: Matrix4) : RenderMesh {

    private val buffer = FloatArray(10)

    var projection: Matrix4 by Delegates.observable(projection) { p, o, n ->
        shapeRenderer.projectionMatrix = n
    }

    private val shapeRenderer = ShapeRenderer().apply {
        setAutoShapeType(true)
    }

    override fun flush() {
        shapeRenderer.flush()
    }

    fun drawRect(x:Float, y:Float, width:Float, height:Float, color:Color) {
        drawRect(
                x, y,
                x + width, y,
                x + width, y + height,
                x, y + height,
                color)
    }

    fun drawRect(x1:Float, y1:Float,
                 x2:Float, y2:Float,
                 x3:Float, y3:Float,
                 x4:Float, y4:Float,
                 color:Color) {

        buffer[0] = x1 ; buffer[1] = y1
        buffer[2] = x2 ; buffer[3] = y2
        buffer[4] = x3 ; buffer[5] = y3
        buffer[6] = x4 ; buffer[7] = y4
        buffer[8] = x1 ; buffer[9] = y1

        if(!shapeRenderer.isDrawing)
            shapeRenderer.begin()

        if(shapeRenderer.color != color)
            shapeRenderer.color = color

        shapeRenderer.polyline(buffer)
    }

}