package ru.nobirds.rx.event

import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.observable.filter
import ru.nobirds.rx.observable.filterIs
import ru.nobirds.rx.observable.subscribe

interface Event {

    var bubbling: Boolean
    var handled: Boolean

}

abstract class AbstractEvent() : Event {

    override var bubbling: Boolean = false
    override var handled: Boolean = false

}

inline fun <reified E:Event> Eventable.onEvent(crossinline handler:(E)->Unit): Subscription
        = events().filterIs<E>().filter { !it.bubbling }.subscribe { handler(it) }

inline fun <reified E:Event> Eventable.onBubbledEvent(noinline handler:(E)->Unit):Subscription
        = events().filter { it.bubbling }.filterIs<E>().subscribe(handler)
