package ru.nobirds.rx.event

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.ComponentProvider
import ru.nobirds.rx.component.RootComponent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module

interface EventTraverseStrategy {

    fun traverse(eventable: Eventable, strategy: TraverseStrategy = TraverseStrategies.LayerByLayer):Sequence<Eventable>

}

object EventTraverseStrategies {

    fun strategy(name:String, fetcher:(eventable: Eventable)->Sequence<Eventable>):EventTraverseStrategy = object : EventTraverseStrategy {

        override fun traverse(eventable: Eventable, strategy: TraverseStrategy): Sequence<Eventable>
                = strategy.build(eventable, fetcher)

        override fun toString(): String = "event traverse strategy [$name]"
    }

    fun composite(name: String, vararg fetcher: (Eventable)->Sequence<Eventable>):EventTraverseStrategy = strategy(name) { e ->
        fetcher.asSequence().flatMap { it(e) }
    }

    object Fetcher {
        val single:(Eventable)->Sequence<Eventable> = { e -> emptySequence() }
        val components:(Eventable)->Sequence<Eventable> = { e -> if (e is ComponentProvider) e.components else emptySequence() }
        val modules:(Eventable)->Sequence<Eventable> = { e -> if (e is ContainerModule) e.children else emptySequence() }
        val parent:(Eventable)->Sequence<Eventable> = { e -> if (e is Module && e.parent != null) sequenceOf(e.parent!!) else emptySequence() }
        val parentComponent:(Eventable)->Sequence<Eventable> = { e ->
            if (e is Component && e.parentComponent != null)
                sequenceOf(e.parentComponent!!)
            else if(e is RootComponent)
                sequenceOf(e.parent)
            else emptySequence()
        }
    }

    val single:EventTraverseStrategy = strategy("single", Fetcher.single)
    val modules:EventTraverseStrategy = strategy("modules", Fetcher.modules)
    val parent:EventTraverseStrategy = strategy("parent", Fetcher.parent)
    val parentComponent:EventTraverseStrategy = strategy("parent component", Fetcher.parentComponent)
    val components:EventTraverseStrategy = strategy("components", Fetcher.components)
    val modulesAndComponents:EventTraverseStrategy =  composite("modules and components", Fetcher.components, Fetcher.modules)

    val parentsAndComponents:EventTraverseStrategy =  composite("parents and components", Fetcher.parent, Fetcher.components)
    val parentAndModulesAndComponents:EventTraverseStrategy = composite("parent, modules and components", Fetcher.components, Fetcher.parentComponent, Fetcher.modules, Fetcher.parent)
    val parentComponentAndModules:EventTraverseStrategy = composite("parent component", Fetcher.parentComponent, Fetcher.parent)

}