package ru.nobirds.rx.event

import ru.nobirds.rx.utils.sequence
import java.util.ArrayDeque
import kotlin.collections.AbstractIterator
import kotlin.collections.isNotEmpty
import kotlin.sequences.sequenceOf

internal class ChildByChildIterator(
        root: Eventable,
        val fetch: (Eventable) -> Sequence<Eventable>) : AbstractIterator<Eventable>() {

    private val stack = ArrayDeque<Iterator<Eventable>>()
    private var iterator = sequenceOf(root).iterator()

    override fun computeNext() {
        if (iterator.hasNext()) {
            val next = iterator.next()

            setNext(next)

            val iterator = fetch(next).iterator()

            if (iterator.hasNext()) {
                stack.push(this.iterator)
                this.iterator = iterator
            }
        } else {
            if (stack.isNotEmpty()) {
                this.iterator = stack.poll()
                computeNext()
            } else {
                done()
            }
        }
    }
}


internal class LayerByLayerIterator(
        root: Eventable,
        val fetch: (Eventable) -> Sequence<Eventable>) : AbstractIterator<Eventable>() {

    private val stack = ArrayDeque<Iterator<Eventable>>()
    private var iterator = sequenceOf(root).iterator()

    override fun computeNext() {
        if (iterator.hasNext()) {
            val next = iterator.next()

            setNext(next)

            val iterator = fetch(next).iterator()

            if (iterator.hasNext()) {
                stack.addLast(iterator)
            }
        } else if(stack.isNotEmpty()) {
            this.iterator = stack.poll()
            computeNext()
        } else {
            done()
        }
    }
}

interface TraverseStrategy {
    fun build(root:Eventable, fetch:(Eventable)->Sequence<Eventable>):Sequence<Eventable>
}

object TraverseStrategies {
    val ChildByChild:TraverseStrategy =  object: TraverseStrategy {
        override fun build(root: Eventable, fetch: (Eventable) -> Sequence<Eventable>): Sequence<Eventable> {
            return sequence { ChildByChildIterator(root, fetch) }
        }
    }

    val LayerByLayer:TraverseStrategy = object: TraverseStrategy {
        override fun build(root: Eventable, fetch: (Eventable) -> Sequence<Eventable>): Sequence<Eventable> {
            return sequence { LayerByLayerIterator(root, fetch) }
        }
    }

}

