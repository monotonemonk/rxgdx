package ru.nobirds.rx.event

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.ObservableConsumer

interface Eventable {

    fun notify(event: Event)

    fun events(): Observable<Event>

}

fun Eventable.fire(event: Event, strategy: EventTraverseStrategy,
                   traverseStrategy: TraverseStrategy = TraverseStrategies.ChildByChild) {
    Eventables.fire(this, event, strategy, traverseStrategy)
}

object Eventables {

    fun fire(root:Eventable, event: Event, strategy: EventTraverseStrategy,
             traverseStrategy: TraverseStrategy = TraverseStrategies.ChildByChild) {
        if(!event.handled) {
            strategy.traverse(root, traverseStrategy).all {
                if(!event.handled) it.notify(event)
                !event.handled
            }
        }
    }

}

open class EventableSupport() : Eventable {

    private val impl = ObservableConsumer<Event>()

    override fun notify(event: Event) {
        impl.onNext(event)
    }

    override fun events(): Observable<Event> = impl

}