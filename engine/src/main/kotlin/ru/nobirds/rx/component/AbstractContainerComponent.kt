package ru.nobirds.rx.component

import ru.nobirds.rx.di.SimpleMutableContext
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.PrepareEvent
import java.util.LinkedHashMap
import kotlin.reflect.KClass

abstract class AbstractContainerComponent(
        val definitionFactory: DefinitionFactory,
        val componentFactory: ComponentFactory) : AbstractComponent(), ContainerComponent {

    private val context = SimpleMutableContext()
    private val container = LinkedHashMap<KClass<out Component>, Component>()

    override val components: Sequence<Component>
        get() = container.values.asSequence()

    override fun contains(component: KClass<out Component>): Boolean {
        return container.contains(component)
    }

    override fun detach(component: KClass<out Component>) {
        val removed = container.remove(component)
        removed?.parentComponent = null
    }

    override fun <C : Component> attach(component: KClass<C>):C {
        val found = get(component)

        if(found != null) return found

        val definition = definitionFactory.create(component)

        val instance = componentFactory.create(definition)

        attach(instance)

        val dependencies = definition.dependencies
                .asSequence()
                .filter { it !in this }

        dependencies.forEach { attach(it) }

        instance.fire(PrepareEvent, EventTraverseStrategies.single)

        return instance
    }

    private fun attach(component: Component) {
        val type = component.javaClass.kotlin

        require(!contains(type)) { "Duplicate component of type $type" }

        container.put(type, component)
        context.register(component)

        component.parentComponent = this
    }

    override operator fun <T : Component> get(type: KClass<T>): T? {
        return container[type] as T?
    }

    override fun <T : Any> find(type: KClass<T>): T? = context.findAll(type).firstOrNull()

}