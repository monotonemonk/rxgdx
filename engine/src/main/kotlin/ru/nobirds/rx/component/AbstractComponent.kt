package ru.nobirds.rx.component

import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.EventableSupport
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.AfterModuleAttachedEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.onDispose
import ru.nobirds.rx.module.onPrepare
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.onChange
import ru.nobirds.rx.property.property

abstract class AbstractComponent() : Component, Eventable by EventableSupport() {

    override val parent: Module?
        get() = parentComponent?.parent

    val parentComponentProperty: Property<ContainerComponent?> = property<ContainerComponent?>(null) {
        onChange { property, old, new -> onChangeParentComponent(old, new) }
    }

    init {
        onDispose {
            dispose()
        }

        onPrepare {
            setup()
        }
    }

    private fun onChangeParentComponent(old: Component?, new: Component?) {
        val isNewParent = old?.parent != new?.parent

        if (isNewParent && old?.parent != null)
            fire(BeforeDetachedFromParentEvent(old!!.parent!!), EventTraverseStrategies.single)

        if (old != null) fire(BeforeComponentDetachedEvent(old), EventTraverseStrategies.single)

        if (new != null) fire(AfterComponentAttachedEvent(new), EventTraverseStrategies.single)

        if (isNewParent && new?.parent != null)
            fire(AfterAttachedToParentEvent(new!!.parent!!), EventTraverseStrategies.single)

        if (parent?.parent != null)
            fire(AfterModuleAttachedEvent, EventTraverseStrategies.components)
    }

    override var parentComponent: ContainerComponent? by parentComponentProperty

    open protected fun setup() {
    }

    open protected fun dispose() {
    }

    override fun toString(): String = "${this.javaClass.simpleName}"

}
