package ru.nobirds.rx.component

import ru.nobirds.rx.module.Module
import kotlin.reflect.KClass

class RootComponent(override val parent: Module,
                    definitionFactory: DefinitionFactory,
                    componentFactory: ComponentFactory) : AbstractContainerComponent(definitionFactory, componentFactory) {

    fun withComponents(vararg components: KClass<out Component>):RootComponent {
        attachAll(*components)
        return this
    }

    fun withComponents(components: Sequence<KClass<out Component>>):RootComponent {
        for (component in components) {
            attach(component)
        }

        return this
    }

}