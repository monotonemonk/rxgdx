package ru.nobirds.rx.component

import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.findById
import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.property.Property
import kotlin.reflect.KClass

interface ComponentProvider {

    val components:Sequence<Component>

    operator fun <T: Component> get(type: KClass<T>):T?

    fun <T: Any> find(type: KClass<T>):T?

    operator fun contains(component:KClass<out Component>):Boolean

    fun <C:Component> attach(component: KClass<C>):C

    fun attachAll(vararg components: KClass<out Component>) {
        for (component in components) {
            attach(component)
        }
    }

    fun detach(component: KClass<out Component>)

    fun detach(component: Component) {
        detach(component.javaClass.kotlin)
    }

}

interface Component : Eventable {

    val parent: Module?

    var parentComponent:ContainerComponent?

    fun detach() {
        parentComponent?.detach(this)
    }

}

interface ContainerComponent : Component, ComponentProvider

class BeforeComponentDetachedEvent(val component: Component) : AbstractEvent()
class AfterComponentAttachedEvent(val component: Component) : AbstractEvent()

class AfterAttachedToParentEvent(val parent: Module) : AbstractEvent()
class BeforeDetachedFromParentEvent(val parent: Module) : AbstractEvent()

inline fun <reified C:Component> Sequence<Module>.filterByComponent():Sequence<Module> = filter { it.hasComponent<C>() }

inline fun <reified T: Component> ComponentProvider.hasComponent():Boolean = findOptionalComponent<T>() != null
inline fun <reified T: Component> ComponentProvider.findComponent():T =
        findOptionalComponent<T>()
                ?: throw IllegalArgumentException("Component of type ${T::class.qualifiedName} not found in container ${this}")

inline fun <reified T: Component> ComponentProvider.findOptionalComponent():T? = get(T::class)
inline fun <reified T: Any> ComponentProvider.findOptionalComponentByInterface():T? = find(T::class)

inline fun <reified C: Component> Component.dependency():Lazy<C> = dependency<C, C> { this }

inline fun <reified C: Component, R> Component.dependency(crossinline accessor:C.()->R):Lazy<R> = lazy {
    val component = parentComponent?.findComponent<C>() ?:
            throw IllegalArgumentException("Not parent component -- no dependency")

    component.accessor()
}

inline fun <reified C: Component, R> Component.dependentProperty(crossinline accessor:C.()->Property<R>):Lazy<Property<R>> = dependency(accessor)

inline fun <reified C: Component> Component.dependencyOptional():Lazy<C?> = lazy {
    if(parentComponent == null)
        throw IllegalArgumentException("Not parent component -- no dependency")

    parentComponent?.findOptionalComponent<C>()
}

fun Component.onAttachedToParent(event:(AfterAttachedToParentEvent)->Unit): Subscription = onEvent(event)
fun Component.onDetachedFromParent(event:(BeforeDetachedFromParentEvent)->Unit):Subscription = onEvent(event)
fun Component.onDetached(event:(BeforeComponentDetachedEvent)->Unit):Subscription = onEvent(event)
fun Component.onAttached(event:(AfterComponentAttachedEvent)->Unit):Subscription = onEvent(event)

inline fun <reified M:Module> Component.injectById(id:String):Lazy<M> = lazy {
    parent?.findById(id, M::class) ?: throw IllegalArgumentException("Module with id $id and type ${M::class} not found.")
}

inline fun <reified M:Module> Component.injectByIdOptional(id:String):Lazy<M?> = lazy {
    parent?.findById(id, M::class)
}

inline fun <reified C:Component> ComponentProvider.attach():C = attach(C::class)
inline fun <reified C:Component> ComponentProvider.attach(initializer:C.()->Unit):C = attach(C::class).apply(initializer)
