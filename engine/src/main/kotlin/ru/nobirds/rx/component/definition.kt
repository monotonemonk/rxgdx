package ru.nobirds.rx.component

import kotlin.reflect.KClass

class ComponentDefinition<C:Component>(
        val type:KClass<C>,
        val dependencies: Set<KClass<out Component>>,
        val children: Set<KClass<out Component>>)

