package ru.nobirds.rx.component

import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.createWithConstructorInjection

interface ComponentFactory {

    fun <C:Component> create(component: ComponentDefinition<C>):C

}

class ReflectionComponentFactory(val context:Context) : ComponentFactory {

    override fun <C:Component> create(component: ComponentDefinition<C>): C {
        val instance = context.createWithConstructorInjection(component.type)

        if (component.children.any()) {
            if(instance is ContainerComponent) {
                for (child in component.children) {
                    instance.attach(child)
                }
            } else throw IllegalStateException("Only container component can have children")
        }

        return instance
    }

}

