package ru.nobirds.rx.component

import ru.nobirds.rx.annotation.DefinitionAnnotations
import ru.nobirds.rx.di.Context
import kotlin.reflect.KClass

/*
interface ComponentDefinitionProvider {

    fun createDefinitions(component: KClass<out Component>): Sequence<ComponentDefinition>

}


abstract class AbstractComponentDefinitionProvider() : ComponentDefinitionProvider {

    override fun createDefinitions(component: KClass<out Component>): Sequence<ComponentDefinition> {
        return fetchComponentTypes(component).map { createDefinition(it) }
    }

    protected abstract fun fetchComponentTypes(component: KClass<out Component>): Sequence<KClass<out Component>>

    private fun createDefinition(type: KClass<out Component>): ComponentDefinition {
        return ComponentDefinition(type,
                findDependentComponents(type).toList())
    }

    protected abstract fun findChildComponents(type: KClass<*>?): Sequence<KClass<out Component>>

    protected abstract fun findDependentComponents(type: KClass<*>): Sequence<KClass<out Component>>

}

abstract class ReflectionComponentDefinitionProvider(vararg val additional: KClass<out Component>) : AbstractComponentDefinitionProvider() {

    override fun fetchComponentTypes(component: KClass<out Component>): Sequence<KClass<out Component>>
            = findDependent(findChildComponents(fetchType(component)).toSet() + additional.toSet(), emptySet())

    abstract protected fun fetchType(component: KClass<out Component>): KClass<*>?

    override fun findChildComponents(type: KClass<*>?): Sequence<KClass<out Component>>
            = if(type != null) DefinitionAnnotations.findChildComponents(type) else emptySequence()

    override fun findDependentComponents(type: KClass<*>): Sequence<KClass<out Component>> {
        return findDependent(type, emptySet())
    }

    private fun findDependent(type: KClass<*>, set: Set<KClass<out Component>>): Sequence<KClass<out Component>> {
        val dependencies = DefinitionAnnotations.findDependentComponents(type).toSet()

        return findDependent(dependencies, set)
    }

    private fun findDependent(dependencies: Set<KClass<out Component>>, set: Set<KClass<out Component>>): Sequence<KClass<out Component>> {
        val onlyNew = dependencies - set

        val all = dependencies + set

        val subDependencies = onlyNew.asSequence().flatMap { findDependent(it, all) }.toSet()

        return (all + subDependencies).asSequence()
    }

}

object NoneComponentDefinitionProvider : ComponentDefinitionProvider {
    override fun createDefinitions(component: KClass<out Component>): Sequence<ComponentDefinition> = emptySequence()
}

class FixedClassComponentDefinitionProvider(val type: KClass<*>, vararg additional:KClass<out Component>) :
        ReflectionComponentDefinitionProvider(*additional) {
    override fun fetchType(component: KClass<out Component>): KClass<*> = type
}

object ComponentTypeDefinitionProvider : ReflectionComponentDefinitionProvider() {
    override fun fetchType(component: KClass<out Component>): KClass<*> = component
}

class SimpleComponentDefinitionProvider(vararg val component: KClass<out Component>):
        ReflectionComponentDefinitionProvider(*component) {
    override fun fetchType(component: KClass<out Component>): KClass<*>? = null
}
*/


/*
interface ComponentFactory {

    fun create(definitions:Sequence<ComponentDefinition>): Sequence<Component>

}

object DefaultComponentFactory : ComponentFactory {

    override fun create(definitions: Sequence<ComponentDefinition>):
            Sequence<Component> {

        val components = definitions.flatMap { it.dependencies.asSequence() + it.type }.toSet()

        return components.asSequence().map { it.create() }
    }

}
*/

interface DependencyResolver {

    fun resolveDependencies(component: KClass<out Component>):Sequence<KClass<out Component>>
    fun resolveChildren(component: KClass<*>):Sequence<KClass<out Component>>

}

object AnnotationDependencyResolver : DependencyResolver {

    override fun resolveDependencies(component: KClass<out Component>): Sequence<KClass<out Component>> {
        return DefinitionAnnotations.findDependentComponents(component)
    }

    override fun resolveChildren(component: KClass<*>): Sequence<KClass<out Component>> {
        return DefinitionAnnotations.findChildComponents(component)
    }

}

interface DefinitionFactory {

    fun <C:Component> create(component: KClass<C>):ComponentDefinition<C>

}

class DefaultDefinitionFactory(val dependencyResolver: DependencyResolver) : DefinitionFactory {

    override fun <C:Component> create(component: KClass<C>): ComponentDefinition<C> {
        return ComponentDefinition(component,
                dependencyResolver.resolveDependencies(component).toSet(),
                dependencyResolver.resolveChildren(component).toSet())
    }

}

