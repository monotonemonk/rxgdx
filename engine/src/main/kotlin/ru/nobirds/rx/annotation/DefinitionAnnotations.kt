package ru.nobirds.rx.annotation

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.di.fetchInstanceTypes
import kotlin.reflect.KClass

object DefinitionAnnotations {

    fun findChildComponents(type: KClass<*>): Sequence<KClass<out Component>> =
            type.java.fetchInstanceTypes().flatMap { it.getAnnotation(ChildComponents::class.java)?.value?.asSequence() ?: emptySequence() }

    fun findDependentComponents(type: KClass<*>): Sequence<KClass<out Component>> =
            type.java.fetchInstanceTypes().flatMap { it.getAnnotation(DependentComponents::class.java)?.value?.asSequence() ?: emptySequence() }

}