package ru.nobirds.rx.annotation

import ru.nobirds.rx.component.Component
import java.lang.annotation.Inherited
import kotlin.reflect.KClass

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@Inherited
annotation class DependentComponents(vararg val value:KClass<out Component>)

