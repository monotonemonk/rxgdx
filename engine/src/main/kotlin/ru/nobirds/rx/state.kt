package ru.nobirds.rx

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.badlogic.gdx.utils.JsonWriter
import java.io.Writer
import java.util.HashMap
import kotlin.reflect.KClass

interface State {

    val name:String

    fun <T:Any> get(name:String, type:KClass<T>, defaultValue:()->T? = { null }):T?
    fun <T:Any> set(name:String, type:KClass<T>, value:()->T? = { null })

    fun sub(name: String):State

    fun writeTo(writer:Writer)

}

abstract class AbstractState<V>(override val name:String) : State {

    protected val values:MutableMap<String, Any?> = HashMap()

    protected var changed = false

    override fun <T : Any> get(name: String, type:KClass<T>, defaultValue: () -> T?): T? {
        val value = values.getOrPut(name) { read(name, type) }
        return value as T? ?: defaultValue()
    }

    abstract protected fun <T:Any> read(name: String, type:KClass<T>): T?

    override fun <T : Any> set(name: String, type:KClass<T>, value: () -> T?) {
        values[name] = value()
        changed = true
    }

}


object JsonConverter {
    fun <T:Any> convertTo(value:JsonValue, type: KClass<T>):T? {
        throw UnsupportedOperationException()
    }

    fun setValue(value:Any?, jsonValue: JsonValue) {
        throw UnsupportedOperationException()
    }
}


class JsonState(val json: JsonValue, name:String) : AbstractState<JsonValue>(name) {

    override fun <T : Any> read(name: String, type: KClass<T>): T? {
        return JsonConverter.convertTo(json.get(name), type)
    }

    override fun writeTo(writer: Writer) {
        flush()
        JsonWriter(writer)
        Json(JsonWriter.OutputType.json)
    }

    private fun flush() {
        if (changed) {
            for ((name, value) in values) {
                write(name, value)
            }
            changed = false
        }
    }

    private fun write(name: String, value: Any?) {
        JsonConverter.setValue(value, json.get(name))
    }

    override fun sub(name: String): State {
        val value = json.get(name)
        return JsonState(value, name)
    }
}