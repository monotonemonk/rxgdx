package ru.nobirds.rx

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.AssetCoordinatorImpl
import ru.nobirds.rx.component.AnnotationDependencyResolver
import ru.nobirds.rx.component.ComponentFactory
import ru.nobirds.rx.component.DefaultDefinitionFactory
import ru.nobirds.rx.component.DefinitionFactory
import ru.nobirds.rx.component.ReflectionComponentFactory
import ru.nobirds.rx.di.Context
import ru.nobirds.rx.di.MutableContext
import ru.nobirds.rx.di.SimpleMutableContext
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.ObservableConsumer
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.getValue
import ru.nobirds.rx.property.map
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.ImmediateRenderConveyor
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.shader.utils.Textures
import ru.nobirds.rx.shader.utils.Gls
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableSize
import ru.nobirds.rx.utils.files.GdxResourceManager
import ru.nobirds.rx.utils.files.ResourceManager
import java.util.HashMap

interface SceneDirector {

    val context:Context

    val renderer: RenderConveyor

    val scene:Stage?

    val resources: ResourceManager

    val assets:AssetCoordinator

    val componentFactory:ComponentFactory
    val componentDefinitionFactory:DefinitionFactory

    val screen: ImmutableSize

    val delta:Float

    var debug:Boolean

    fun addScene(name:String, scene:()->Stage)

    fun closeScene(name: String)

    fun removeScene(name: String)

    fun scheduleScene(name: String, deletePrevious: Boolean = false): Observable<Stage>

    fun addInputTranslator(translator:(()->Eventable?)->InputProcessor)

}

object DirectorProvider {

    private var sceneDirectorFactory:()-> SceneDirector = { throw IllegalStateException("Context not initialized") }

    private val instance by lazy { sceneDirectorFactory() }

    fun factory(factory:()-> SceneDirector) {
        this.sceneDirectorFactory = factory
    }

    fun get(): SceneDirector = instance

}

internal data class NameAndScene(val name:String, val scene:Stage)

internal data class SceneAndDeletePrevious(val scene:NameAndScene, val deletePrevious: Boolean, val observable: ObservableConsumer<Stage>) {

    fun notifyComplete() {
        observable.onNext(scene.scene)
        observable.complete()
    }

}

fun director(initializer:SceneDirector.()->Unit):ApplicationListener = GenericSceneDirector(initializer)

open class GenericSceneDirector(val initializer:GenericSceneDirector.()->Unit) : SceneDirector, ApplicationListener {

    override val renderer: RenderConveyor by lazy { ImmediateRenderConveyor() }

    override val resources: ResourceManager = GdxResourceManager()

    override val assets: AssetCoordinator = AssetCoordinatorImpl(4, resources)

    override val context: MutableContext = SimpleMutableContext()

    override val componentFactory: ComponentFactory = ReflectionComponentFactory(context)
    override val componentDefinitionFactory: DefinitionFactory = DefaultDefinitionFactory(AnnotationDependencyResolver)

    private val scenes = HashMap<String, ()->Stage>()

    private val loadedScenes = HashMap<String, Stage>()

    private var schedulesScene:SceneAndDeletePrevious? = null

    var pauseSceneName:String? = null

    private var pausedSceneName:String? = null

    private val currentSceneProperty:Property<NameAndScene?> = property(null)
    private var currentScene:NameAndScene? by currentSceneProperty

    override val scene: Stage? by currentSceneProperty.map { it?.scene }

    private val screenImpl: MutableSize = MutableSize.zero()

    override val screen: ImmutableSize
        get() = screenImpl

    private val inputProcessor = InputMultiplexer()

    override val delta: Float
        get() = Gdx.graphics.deltaTime

    override var debug: Boolean = false

    init {
        setupDefaultContext()
    }

    private fun setupDefaultContext() {
        with(context) {
            register("assets", assets)
            register("resources", resources)
            register("screen", screen)
            register("director", this@GenericSceneDirector)
        }
    }

    override fun addInputTranslator(translator: (() -> Eventable?) -> InputProcessor) {
        inputProcessor.addProcessor(translator { scene })
    }

    private fun setupInputProcessor() {
        val processor = Gdx.input.inputProcessor

        if(processor != null)
            inputProcessor.addProcessor(processor)

        Gdx.input.inputProcessor = inputProcessor
    }

    private fun removeInputProcessor() {
        val processor = Gdx.input.inputProcessor
        if (processor is InputMultiplexer) {
            processor.clear()
        }
    }

    override fun addScene(name: String, scene: ()->Stage) {
        if (name in scenes)
            throw IllegalArgumentException("Scene with name $name already registered, remove it before")

        scenes.put(name, scene)
    }

    override fun closeScene(name: String) {
        loadedScenes.remove(name)?.dispose()
    }

    override fun removeScene(name: String) {
        closeScene(name)
        scenes.remove(name)
    }

    override fun scheduleScene(name: String, deletePrevious: Boolean):Observable<Stage> {
        val scene = findSceneByName(name)

        val promise = ObservableConsumer<Stage>()

        this.schedulesScene = SceneAndDeletePrevious(NameAndScene(name, scene), deletePrevious, promise)

        return promise
    }

    private fun findSceneByName(name: String): Stage {
        val loaded = loadedScenes[name]

        if(loaded != null)
            return loaded

        val stage = scenes[name] ?:
                throw IllegalArgumentException("Stage with name $name not found. Available stages: ${scenes.keys.joinToString()}")

        val scene = stage()

        this.loadedScenes[name] = scene

        return scene
    }

    override fun resize(width: Int, height: Int) {
        screenImpl.size(width.toFloat(), height.toFloat())

        notifyResize()
    }

    private fun notifyResize() {
        scene?.fire(ResizeEvent(screen.width, screen.height), EventTraverseStrategies.modulesAndComponents)
    }

    override fun create() {
        DirectorProvider.factory { this }
        setupInputProcessor()
        initializer()
    }

    override fun render() {
        Textures.units.clean()

        checkChangeScene()

        updateScene()

        renderScene()
    }

    private fun renderScene() {
        Gls.clear()
        scene?.render(renderer)
    }

    private fun updateScene() {
        scene?.update(delta)
    }

    private fun checkChangeScene() {
        val next = schedulesScene
        if (next != null && next.scene.scene.load()) {

            // need unload prev scene
            val old = this.currentScene
            if (next.deletePrevious && old != null) {
                loadedScenes.remove(old.name)
                old.scene.dispose()
            }

            this.currentScene = next.scene
            this.schedulesScene = null

            notifyResize()

            next.notifyComplete()
        }
    }

    override fun pause() {
        scene?.fire(PauseEvent, EventTraverseStrategies.modulesAndComponents)

        if (pauseSceneName != null) {
            pausedSceneName = currentScene?.name
            scheduleScene(pauseSceneName!!)
        }
    }

    override fun resume() {
        if (pausedSceneName != null) {
            scheduleScene(pausedSceneName!!)
            pausedSceneName = null
        }

        scene?.fire(ResumeEvent, EventTraverseStrategies.modulesAndComponents)
    }

    override fun dispose() {
        currentScene = null
        loadedScenes.values.forEach { it.dispose() }
        removeInputProcessor()
    }

}

val Director: SceneDirector
    get() = DirectorProvider.get()

