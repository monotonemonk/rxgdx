package ru.nobirds.rx.action

class SequentialAction(val actions:List<Action>) : Action {

    private var iterator = actions.iterator()

    private var current: Action? = null

    override fun process(delta: Float): Boolean {
        if(current == null && !iterator.hasNext())
            return true

        if (current == null) {
            current = iterator.next()
        }

        if (current!!.process(delta)) {
            if(iterator.hasNext())
                current = iterator.next()
            else
                return true
        }

        return false
    }

    override fun renew() {
        this.actions.forEach { it.renew() }
        this.iterator = actions.iterator()
        this.current = null
    }

}