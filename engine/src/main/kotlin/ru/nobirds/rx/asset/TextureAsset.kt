package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.TextureData
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.map
import ru.nobirds.rx.observable.withRunner
import ru.nobirds.rx.utils.observable.GdxRunner
import ru.nobirds.rx.utils.files.ResourceManager

data class TextureAssetParameters(
        val name:String, val format: Pixmap.Format? = null, val genMipMap:Boolean = false) : AssetParameters<Texture>

class TextureAsset(parameters: TextureAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<Texture, TextureAssetParameters>(parameters) {

    override fun load(observable: Observable<TextureAssetParameters>): Observable<Texture> =
            observable
                    .withRunner(assetCoordinator.newThreadRunner).map { loadTextureData(it) }
                    .withRunner(GdxRunner.runner).map { Texture(it) }

    private fun loadTextureData(it: TextureAssetParameters) = TextureData.Factory
            .loadFromFile(resources.forRead(it.name).toFileHandle(), it.format, it.genMipMap)
            .apply { if (!isPrepared) prepare() }

}
