package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.Pixmap
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.flatMap
import ru.nobirds.rx.observable.map
import ru.nobirds.rx.observable.withRunner
import ru.nobirds.rx.utils.observable.GdxRunner
import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.files.readBytes
import ru.nobirds.rx.utils.ninepatch.NinePatch
import ru.nobirds.rx.utils.ninepatch.NinePatchBounds
import ru.nobirds.rx.utils.ninepatch.NinePatchFactory

data class BitmapAssetParameters(val file: String) : AssetParameters<Pixmap>

class BitmapAsset(parameters: BitmapAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<Pixmap, BitmapAssetParameters>(parameters) {

    override fun load(observable: Observable<BitmapAssetParameters>): Observable<Pixmap> = observable
        .withRunner(assetCoordinator.newThreadRunner).map { resources.forRead(it.file).readBytes().let { bytes -> Pixmap(bytes, 0, bytes.size) } }

}

data class NinePatchFactoryAssetParameters(val bitmap: BitmapAssetParameters) : AssetParameters<NinePatchFactory>

class NinePatchFactoryAsset(parameters: NinePatchFactoryAssetParameters, val assetCoordinator: AssetCoordinator, resources: ResourceManager) :
        AbstractAsset<NinePatchFactory, NinePatchFactoryAssetParameters>(parameters) {

    override fun load(observable: Observable<NinePatchFactoryAssetParameters>): Observable<NinePatchFactory> = observable
            .flatMap { assetCoordinator.require(it.bitmap) }
            .withRunner(assetCoordinator.newThreadRunner).map { NinePatchFactory(it) }

}

data class NinePatchAssetParameters(val factory: NinePatchFactoryAssetParameters, val bounds: NinePatchBounds? = null) : AssetParameters<NinePatch>

class NinePatchAsset(parameters: NinePatchAssetParameters, val assetCoordinator: AssetCoordinator, resources: ResourceManager) :
        AbstractAsset<NinePatch, NinePatchAssetParameters>(parameters) {

    override fun load(observable: Observable<NinePatchAssetParameters>): Observable<NinePatch> = observable
            .flatMap { assetCoordinator.require(it.factory) }
            .withRunner(GdxRunner.runner).map { if(parameters.bounds != null) it.createNinePatch(parameters.bounds) else it.createNinePatch() }

}

