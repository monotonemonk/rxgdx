package ru.nobirds.rx.asset

import ru.nobirds.rx.utils.files.ResourceManager
import ru.nobirds.rx.utils.instanceOf
import java.util.ArrayList
import kotlin.reflect.KClass

interface AssetFactory {

    fun support(parameters: AssetParameters<*>):Boolean

    fun createAsset(parameters: AssetParameters<*>, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<*, *>

}

inline fun <T:Any, reified P: AssetParameters<T>> assetFactory(noinline factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>): AssetFactory =
        SimpleAssetFactory(P::class, factory)

class SimpleAssetFactory<T:Any, P: AssetParameters<T>>(val type: KClass<P>, val factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>) : AssetFactory {

    override fun support(parameters: AssetParameters<*>): Boolean
            = parameters instanceOf type

    override fun createAsset(parameters: AssetParameters<*>, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<*, *> =
            factory(parameters as P, assetCoordinator, resources)

}

object DefaultAssetFactories {

    fun createProviderWithDefaults(): AssetProvider = AssetProvider().apply {
        register(this)
    }

    fun register(assetProvider: AssetProvider) {
        defaultFactories().forEach { assetProvider.register(it) }
    }

    private fun defaultFactories(): Sequence<AssetFactory> = sequenceOf(
            assetFactory(::TextureAsset),
            assetFactory(::TextureRegionAsset),
            assetFactory(::FontGeneratorAsset),
            assetFactory(::FontAsset),
            assetFactory(::BitmapAsset),
            assetFactory(::BitmapFontAsset),
            assetFactory(::NinePatchFactoryAsset),
            assetFactory(::NinePatchAsset)
    )

}

class AssetProvider() {

    private val registry = ArrayList<AssetFactory>()

    fun register(factory: AssetFactory) {
        registry.add(factory)
    }

    inline fun <T:Any, reified P: AssetParameters<T>> register(noinline factory:(P, AssetCoordinator, ResourceManager)-> Asset<T, P>) {
        register(assetFactory(factory))
    }

    fun createAssetByParameters(parameters: AssetParameters<*>, assetCoordinator: AssetCoordinator, resources: ResourceManager): Asset<*, *> =
            registry.firstOrNull { it.support(parameters) }?.createAsset(parameters, assetCoordinator, resources) ?:
                    throw IllegalArgumentException("Unsupported asset: $parameters")

}