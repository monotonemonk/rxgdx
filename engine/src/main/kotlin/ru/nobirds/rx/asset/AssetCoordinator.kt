package ru.nobirds.rx.asset

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Runner
import ru.nobirds.rx.observable.Runners
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.utils.files.ResourceManager
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

interface AssetCoordinator {

    val newThreadRunner:Runner

    val assetProvider: AssetProvider

    fun <T:Any> require(parameters: AssetParameters<T>):Observable<T>

    fun <T:Any> require(parameters: AssetParameters<T>, onLoad: (T)->Unit):Observable<T> = require(parameters).apply {
        subscribe(onLoad)
    }

    fun notRequired(parameters: AssetParameters<*>)

}

class AssetCoordinatorImpl(threads:Int = 4,
                           val resources: ResourceManager,
                           override val assetProvider: AssetProvider = DefaultAssetFactories.createProviderWithDefaults()) : AssetCoordinator {

    private val executorService:ExecutorService = Executors.newFixedThreadPool(threads)

    override val newThreadRunner: Runner = Runners.newThread(executorService)

    private val registry = ConcurrentHashMap<AssetParameters<*>, Asset<*,*>>()

    override fun <T : Any> require(parameters: AssetParameters<T>): Observable<T> {
        val asset = asset(parameters) as Asset<T, AssetParameters<T>>

        return asset.get()
    }

    private fun <T : Any> asset(parameters: AssetParameters<T>) = registry.getOrPut(parameters)
        { assetProvider.createAssetByParameters(parameters, this, resources) }

    override fun notRequired(parameters: AssetParameters<*>) {
        val asset = registry[parameters]

        if (asset != null) {
            asset.retain()
            if(!asset.used)
                registry.remove(parameters)
        }
    }

}
