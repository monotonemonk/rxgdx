package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.g2d.TextureRegion
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.flatMap
import ru.nobirds.rx.observable.map
import ru.nobirds.rx.observable.withRunner
import ru.nobirds.rx.utils.observable.GdxRunner
import ru.nobirds.rx.utils.files.ResourceManager

data class TextureRegionAssetParameters(val texture: TextureAssetParameters,
                                        val x:Int, val y:Int, val width:Int, val height:Int) : AssetParameters<TextureRegion>

class TextureRegionAsset(parameters: TextureRegionAssetParameters, val assetCoordinator: AssetCoordinator, resources: ResourceManager) :
        AbstractAsset<TextureRegion, TextureRegionAssetParameters>(parameters) {

    override fun load(observable: Observable<TextureRegionAssetParameters>): Observable<TextureRegion> =
            observable
                    .flatMap { assetCoordinator.require(parameters.texture) }
                    .withRunner(GdxRunner.runner).map { TextureRegion(it, parameters.x, parameters.y, parameters.width, parameters.height) }

}
