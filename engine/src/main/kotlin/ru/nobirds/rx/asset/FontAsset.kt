package ru.nobirds.rx.asset

import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.flatMap
import ru.nobirds.rx.observable.map
import ru.nobirds.rx.observable.withRunner
import ru.nobirds.rx.utils.observable.GdxRunner
import ru.nobirds.rx.utils.files.ResourceManager

data class FontGeneratorAssetParameters(val file: String) : AssetParameters<FreeTypeFontGenerator>

class FontGeneratorAsset(parameters: FontGeneratorAssetParameters, assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<FreeTypeFontGenerator, FontGeneratorAssetParameters>(parameters) {

    override fun load(observable: Observable<FontGeneratorAssetParameters>): Observable<FreeTypeFontGenerator> =
            observable.withRunner(GdxRunner.runner).map { FreeTypeFontGenerator(resources.forRead(parameters.file).toFileHandle()) }

}

data class FontAssetParameters(val generator: FontGeneratorAssetParameters, val parameters: FreeTypeFontGenerator.FreeTypeFontParameter) : AssetParameters<BitmapFont>

class FontAsset(parameters: FontAssetParameters, val assetCoordinator: AssetCoordinator, resources: ResourceManager) : AbstractAsset<BitmapFont, FontAssetParameters>(parameters) {

    override fun load(observable: Observable<FontAssetParameters>): Observable<BitmapFont> =
            observable
                    .flatMap { assetCoordinator.require(parameters.generator) }
                    .withRunner(GdxRunner.runner).map { it.generateFont(parameters.parameters) }

}

data class BitmapFontAssetParameters(val fontFile:String,
                                val imageFile:String,
                                val flip:Boolean = false,
                                val integer:Boolean = true) : AssetParameters<BitmapFont>

class BitmapFontAsset(parameters: BitmapFontAssetParameters, val assetCoordinator: AssetCoordinator, val resources: ResourceManager) :
        AbstractAsset<BitmapFont, BitmapFontAssetParameters>(parameters) {

    override fun load(observable: Observable<BitmapFontAssetParameters>): Observable<BitmapFont> = observable
            .withRunner(assetCoordinator.newThreadRunner).map {
        BitmapFont.BitmapFontData(resources.forRead(it.fontFile).toFileHandle(), it.flip)
    }.withRunner(GdxRunner.runner).map { BitmapFont(it, null as TextureRegion?, parameters.integer) }

}