package ru.nobirds.rx.asset

import com.badlogic.gdx.utils.Disposable
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.observable.cached
import ru.nobirds.rx.observable.observableOf
import ru.nobirds.rx.observable.subscribe

interface AssetParameters<T:Any>

interface Asset<T:Any, P:AssetParameters<T>> : Disposable {

    val used:Boolean

    val parameters:P

    fun get():Observable<T>

    fun retain()

}

abstract class AbstractAsset<T:Any, P:AssetParameters<T>>(override val parameters: P) : Asset<T, P> {

    private var counter:Int = 0

    protected var asset:T? = null

    protected val consumer = observableOf(parameters)

    override val used: Boolean
        get() = counter > 0

    private val observable by lazy { load(consumer).cached() }

    private val subscription:Subscription by lazy { observable.subscribe { asset = it } }

    override fun get(): Observable<T> {
        counter++
        return observable
    }

    abstract protected fun load(observable:Observable<P>):Observable<T>

    override fun dispose() {
        val asset = asset

        if (asset != null) {
            disposeImpl(asset)

            if(asset is Disposable)
                asset.dispose()
        }

        subscription.unsubscribe()
    }

    override fun retain() {
        counter--
    }

    open protected fun disposeImpl(asset: T) {
        // do nothing
    }

}

