package ru.nobirds.rx.module

import ru.nobirds.rx.Director
import ru.nobirds.rx.component.AnnotationDependencyResolver
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.RootComponent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.EventableSupport
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.onChange
import ru.nobirds.rx.property.property
import java.util.ArrayList
import java.util.HashMap
import java.util.TreeMap
import kotlin.reflect.KClass

internal class ModuleContainer {

    private var lastOrder = 0

    private val index = HashMap<Module, Int>()

    private val items = TreeMap<Int, MutableList<Module>>()

    val modules:Sequence<Module>
        get() = items.values.asSequence().flatMap { it.asSequence() }

    fun add(module: Module, order: Int) {
        index.put(module, order)
        items.getOrPut(order) { ArrayList() }.add(module)

        if (order > lastOrder) {
            lastOrder = order
        }
    }

    fun add(module: Module) {
        require(lastOrder < Int.MAX_VALUE)

        add(module, ++lastOrder)
    }

    fun remove(module: Module) {
        val order = index.remove(module)
        if (order != null) {
            items[order]?.remove(module)
        }
    }

    fun change(module: Module, order: Int) {
        remove(module)
        add(module, order)
    }

}



abstract class AbstractModule(id:String? = null, vararg val moduleComponents: KClass<out Component>) :
        Eventable by EventableSupport(), Module {

    private val rootComponent: RootComponent = RootComponent(this,
            Director.componentDefinitionFactory, // todo?
            Director.componentFactory            // todo?
    )

    val idProperty: Property<String?> = property(id)
    override var id:String? by idProperty

    val parentProperty: Property<ContainerModule?> = property<ContainerModule?>(null) {
        onChange { property, old, new ->
            old?.detach(this@AbstractModule)
            if(new != null && new.isAttachedToScene && !(old?.isAttachedToScene?:false))
                fire(AfterModuleAttachedToSceneEvent, EventTraverseStrategies.modulesAndComponents)
        }
    }
    override var parent: ContainerModule? by parentProperty

    init {
        rootComponent
                .withComponents(AnnotationDependencyResolver.resolveChildren(this.javaClass.kotlin))
                .withComponents(*moduleComponents)
    }

    override val components: Sequence<Component>
        get() = rootComponent.components

    override fun <T : Component> get(type: KClass<T>): T? = rootComponent[type]

    override fun <T : Any> find(type: KClass<T>): T? = rootComponent.find(type)

    override fun contains(component: KClass<out Component>): Boolean = rootComponent.contains(component)

    override fun <C : Component> attach(component: KClass<C>): C = rootComponent.attach(component)

    override fun detach(component: KClass<out Component>) {
        rootComponent.detach(component)
    }


    override fun toString(): String =
            "${this.javaClass.simpleName} #${id?:"none"} [${components.joinToString(", ")}]"

}

abstract class AbstractContainerModule(id:String? = null, vararg moduleComponents: KClass<out Component>) :
        AbstractModule(id, *moduleComponents), Eventable by EventableSupport(), ContainerModule {

    private val childrenModules = ModuleContainer()

    override val children: Sequence<Module>
        get() = childrenModules.modules

    override fun attach(module: Module, order:Int?) {
        if(order != null) childrenModules.add(module, order)
        else childrenModules.add(module)

        module.parent = this
        module.fire(AfterModuleAttachedEvent, EventTraverseStrategies.components)
        this.fire(ChildModuleAttached(module), EventTraverseStrategies.components)
    }

    override fun changeOrder(module: Module, order: Int) {
        childrenModules.change(module, order)
    }

    override fun detach(module: Module) {
        this.fire(ChildModuleDetached(module), EventTraverseStrategies.components)
        module.fire(BeforeModuleDetachedEvent, EventTraverseStrategies.components)
        module.parent = null
        childrenModules.remove(module)
    }

}