package ru.nobirds.rx.module

import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.ComponentProvider
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.findStage
import ru.nobirds.rx.observable.Subscription
import kotlin.reflect.KClass

interface Module : ComponentProvider, Eventable {

    val id:String?

    var parent: ContainerModule?

    val isAttached:Boolean
        get() = parent != null

    val isAttachedToScene:Boolean
        get() = parent?.isAttachedToScene ?: false

    fun detach() {
        parent?.detach(this)
    }

}

interface ContainerModule : Module {

    val children:Sequence<Module>

    fun attach(module: Module, order:Int? = null)

    fun changeOrder(module: Module, order:Int)

    fun detach(module: Module)

}

object AfterModuleAttachedEvent : AbstractEvent()
object AfterModuleAttachedToSceneEvent : AbstractEvent()
object BeforeModuleDetachedEvent : AbstractEvent()

class ChildModuleAttached(val module: Module) : AbstractEvent()
class ChildModuleDetached(val module: Module) : AbstractEvent()

fun Module.onAttached(handler:(AfterModuleAttachedEvent)->Unit): Subscription = onEvent(handler)
fun Module.onAttachedToScene(handler:(AfterModuleAttachedToSceneEvent)->Unit): Subscription = onEvent(handler)
fun Module.onDetached(handler:(BeforeModuleDetachedEvent)->Unit): Subscription = onEvent(handler)

object PrepareEvent : AbstractEvent()
object DisposeEvent : AbstractEvent()

fun Eventable.onPrepare(handler:()->Unit): Subscription = onEvent<PrepareEvent> { handler() }
fun Eventable.onDispose(handler:()->Unit): Subscription = onEvent<DisposeEvent> { handler() }

fun Module.parents(): Sequence<Module> =
        if (this.parent != null) generateSequence(this.parent!!) { it.parent } else emptySequence()

fun Module.thisAndParents(): Sequence<Module> =
        sequenceOf(this) + if (this.parent != null) generateSequence(this.parent!!) { it.parent } else emptySequence()

val ContainerModule.containers:Sequence<ContainerModule>
    get() = children.filterIsInstance<ContainerModule>()

fun <M:Module> ContainerModule.findChildById(id:String, type:KClass<M>): M? {
    return if(this.id == id && type.java.isAssignableFrom(this.javaClass)) this as M
    else containers.map { it.findChildById(id, type) }.firstOrNull { it != null }
}

inline fun <reified M:Module> Module.injectById(id:String):Lazy<M> = lazy {
    findById(id, M::class) ?: throw IllegalArgumentException("Module with id $id not found")
}

inline fun <reified M:Module> Module.injectByIdOptional(id:String):Lazy<M?> = lazy {
    findById(id, M::class)
}

fun <M:Module> Module.findById(id:String, type:KClass<M>):M? = findStage()?.findChildById(id, type)

inline fun <reified C:Component> Module.dependency():Lazy<C> = lazy { findComponent<C>() }

