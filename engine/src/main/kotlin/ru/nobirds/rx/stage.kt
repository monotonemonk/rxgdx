package ru.nobirds.rx

import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.parents
import ru.nobirds.rx.render.RenderConveyor

interface Stage : ContainerModule {

    val layers:Sequence<Layer>

    fun dispose()

    fun load():Boolean

    fun update(delta: Float)

    fun render(renderer: RenderConveyor)

    val loaded:Boolean

}

fun Module.findStage():Stage? = parents().filterIsInstance<Stage>().firstOrNull()
