package ru.nobirds.rx

import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.thisAndParents
import ru.nobirds.rx.render.RenderConveyor

interface Layer : ContainerModule {

    var projection: Projection

    var debug:Boolean

    fun render(renderer: RenderConveyor)

    fun renderDebug(renderer: RenderConveyor)

}

fun Module.findLayer():Layer? = thisAndParents().filterIsInstance<Layer>().firstOrNull()
