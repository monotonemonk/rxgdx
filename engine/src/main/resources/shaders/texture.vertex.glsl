attribute vec2 a_position;
attribute vec2 a_texture_position;
attribute vec4 a_tint_color;

uniform mat4 u_projection;

varying vec2 v_position;
varying vec4 v_tint_color;

void main()
{
   v_position = a_texture_position;
   v_tint_color = a_tint_color;

   gl_Position = u_projection * vec4(a_position, 0.0, 1.0);
}
