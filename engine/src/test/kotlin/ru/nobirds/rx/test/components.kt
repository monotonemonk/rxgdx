package ru.nobirds.rx.test

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.DirectorProvider
import ru.nobirds.rx.GenericSceneDirector
import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.*
import ru.nobirds.rx.di.SimpleMutableContext
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.dependency

class TestComponent() : AbstractComponent() {

    fun blabla():String = "test string"

}

@DependentComponents(TestComponent4::class)
@ChildComponents(TestComponent3::class)
class TestComponent2() : AbstractContainerComponent(
        DefaultDefinitionFactory(AnnotationDependencyResolver),
        ReflectionComponentFactory(SimpleMutableContext()))

class TestComponent3() : AbstractComponent()

@DependentComponents(TestComponent5::class)
class TestComponent4() : AbstractComponent()

class TestComponent5() : AbstractComponent()

@ChildComponents(TestComponent::class, TestComponent2::class, TestComponent4::class)
class TestModule() : AbstractModule("test") {

    private val test:TestComponent by dependency()

    fun foo():String = test.blabla()

}

class ComponentsTest {

    init {
        DirectorProvider.factory {
            GenericSceneDirector {  }
        }
    }

    @Test
    fun test1() {
        val testModule = TestModule()

        val components = testModule.components.toList()

        Assert.assertEquals(4, components.size)
        Assert.assertEquals("test string", testModule.foo())

        val count = testModule.findComponent<TestComponent2>().components.count()
        Assert.assertEquals(1, count)
    }

}