package ru.nobirds.rx.ui

import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.module.AbstractContainerModule
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.module.dependency
import kotlin.reflect.KClass

@ChildComponents(TransformComponent::class, BoundsComponent::class)
open class UiContainerModule(id: String? = null, vararg moduleComponents: KClass<out Component>) :
        AbstractContainerModule(id, *moduleComponents) {

    val transform:TransformComponent by dependency()
    val bounds:BoundsComponent by dependency()

}


@ChildComponents(TransformComponent::class, BoundsComponent::class)
open class UiModule(id: String? = null, vararg moduleComponents: KClass<out Component>) :
        AbstractModule(id, *moduleComponents) {

    val transform:TransformComponent by dependency()
    val bounds:BoundsComponent by dependency()

}

