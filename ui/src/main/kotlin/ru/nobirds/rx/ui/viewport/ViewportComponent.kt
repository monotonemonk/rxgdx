package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.Director
import ru.nobirds.rx.Projection
import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.Renderable
import ru.nobirds.rx.render.onRender
import ru.nobirds.rx.shader.utils.Gls
import ru.nobirds.rx.utils.ImmutableBounds
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.MutableSize

class ViewportChangedEvent(val viewport: ImmutableBounds) : AbstractEvent()

class ViewportComponent() : AbstractComponent(), MutableBounds, Invalidatable, Renderable {

    val xProperty:Property<Float> = property(0f)
    override var x:Float by xProperty

    val yProperty:Property<Float> = property(0f)
    override var y:Float by yProperty

    val widthProperty:Property<Float> = property(0f)
    override var width:Float by widthProperty

    val heightProperty:Property<Float> = property(0f)
    override var height:Float by heightProperty

    override val invalidated: Observable<Signal> = changed(xProperty, yProperty, widthProperty, heightProperty)

    private val viewportInvalidated = invalidated(false)

    override fun setup() {
        viewportInvalidated.bindOneWaySignal(invalidated)

        onRender {
            it.renderer.render(this)
        }

        onUpdate {
            viewportInvalidated.with {
                updateViewport()
            }
        }
    }

    private fun updateViewport() {
        parent?.fire(ViewportChangedEvent(this), EventTraverseStrategies.modulesAndComponents)
    }

    override fun render(projection: Projection) {
        Gls.viewport(x, y, width, height)
    }

}


interface ViewportScaleStrategy {

    fun scale(world: ImmutableSize, screen: ImmutableSize, viewport: MutableBounds)

}

fun boundsScaleStrategy(scale:(ImmutableSize, ImmutableSize)-> ImmutableBounds): ViewportScaleStrategy = object : ViewportScaleStrategy {
    override fun scale(world: ImmutableSize, screen: ImmutableSize, viewport: MutableBounds) {
        viewport.bounds(scale(world, screen))
    }
}

object Strategies {

    val none = boundsScaleStrategy { world, screen -> ImmutableBounds.readonly(screen) }
    val fillX = boundsScaleStrategy { world, screen ->
        val k = screen.width / world.width
        ImmutableBounds.readonly(world.width * k, world.height * k)
    }

    // todo!!
}

@DependentComponents(ViewportComponent::class)
class ViewportScaleComponent() : AbstractComponent() {

    private val viewport:ViewportComponent by dependency()

    private var world: MutableSize = MutableSize.zero()

    var scale: ViewportScaleStrategy = Strategies.none

    private val invalidatedWorld = invalidated(false)

    override fun setup() {
        onEvent<ResizeEvent> {
            invalidatedWorld.invalidate()
        }

        onEvent<WorldSizeChangedEvent> {
            world.size(it)
            invalidatedWorld.invalidate()
        }

        onUpdate {
            invalidatedWorld.with {
                updateBounds()
            }
        }
    }

    private fun updateBounds() {
        scale.scale(world, Director.screen, viewport)
    }

}

