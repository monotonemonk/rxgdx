package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.ui.BoundsComponent

@DependentComponents(BoundsComponent::class)
class FitToScreenComponent() : AbstractComponent() {

    private val bounds: BoundsComponent by dependency()

    override fun setup() {
        onEvent<ResizeEvent> {
            bounds.bounds(0f, 0f, it.width, it.height)
        }
    }

}