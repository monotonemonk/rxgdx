package ru.nobirds.rx.ui.debug

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.BitmapFontAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.DebugRenderEvent
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.render
import ru.nobirds.rx.render.renderRect
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.TransformComponent

@DependentComponents(BoundsComponent::class)
class DebugBoundsRenderComponent(val assets:AssetCoordinator) : AbstractComponent(), DebugComponent {

    private val fontParameters = BitmapFontAssetParameters(
            "com/badlogic/gdx/utils/arial-15.fnt",
            "com/badlogic/gdx/utils/arial-15.png"
    )

    private var font:BitmapFont? = null
    private val layout:GlyphLayout = GlyphLayout()

    val colorProperty: Property<Color> = property(Color.GREEN)
    var color: Color by colorProperty

    val enabledProperty: Property<Boolean> = property(true)
    override var enabled: Boolean by enabledProperty

    val showBoundsProperty: Property<Boolean> = property(false)
    var showBounds: Boolean by showBoundsProperty

    private val bounds: BoundsComponent by dependency()
    private val transform: TransformComponent by dependency()

    private val boundsInvalidated = invalidated(true)

    override fun setup() {
        onEvent<DebugRenderEvent> {
            render(it.renderer)
        }

        val parentId = parent?.id

        requestFont()

        boundsInvalidated.bindOneWaySignal(bounds.invalidated)

        onUpdate {
            if(font != null)
                boundsInvalidated.with {
                    this.layout.setText(font, computeDebugText(parentId), color, 0f, Align.left, false)
                }
        }
    }

    private fun computeDebugText(parentId: String?) = "${parentId ?: ""}" +
            if(showBounds) "\n[${bounds.x},${bounds.y},${bounds.width},${bounds.height}]" else ""

    private fun requestFont() {
        assets.require(fontParameters) {
            this.font = it
            boundsInvalidated.invalidate()
        }
    }

    fun render(batch: RenderConveyor) {
        if(enabled) {
            batch.renderRect(transform, bounds, color)

            font?.let { font ->
                batch.render(font, layout, transform)
            }
        }
    }

}