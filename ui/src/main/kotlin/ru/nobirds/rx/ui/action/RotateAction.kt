package ru.nobirds.rx.ui.action

import com.badlogic.gdx.math.Interpolation
import ru.nobirds.rx.action.AbstractRelativeTemporalAction
import ru.nobirds.rx.action.ActionBuilder
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.utils.rotate

class RotateAction(val target: Module, val rotation:Float, duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractRelativeTemporalAction(duration, interpolation) {

    private val transform: TransformComponent = target.findComponent()

    override fun delta(percentDelta: Float) {
        transform.rotate(percentDelta * rotation)
    }

}

fun ActionBuilder.rotate(rotation:Float, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    val module = component.parent ?: throw IllegalStateException("Component need to be attached to module")

    action(RotateAction(module, rotation, duration, interpolation))
}