package ru.nobirds.rx.ui.alignment

import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PivotComponent

@DependentComponents(BoundsComponent::class, PivotComponent::class)
class PivotAtCenterOfBoundsComponent() : AbstractComponent() {

    private val bounds: BoundsComponent by dependency()
    private val pivot: PivotComponent by dependency()

    override fun setup() {
        bounds.invalidated.subscribe {
            pivot.position(bounds.width/2f, bounds.height/2f)
        }
    }
}
