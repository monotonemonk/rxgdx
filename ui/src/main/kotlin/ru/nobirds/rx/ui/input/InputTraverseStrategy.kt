package ru.nobirds.rx.ui.input

import ru.nobirds.rx.component.findOptionalComponentByInterface
import ru.nobirds.rx.event.Eventable
import ru.nobirds.rx.event.TraverseStrategy
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.utils.ImmutableBounds

class InputTraverseStrategy(val x:Float, val y:Float) : TraverseStrategy {

    override fun build(root: Eventable, fetch: (Eventable) -> Sequence<Eventable>): Sequence<Eventable> {

        if (root is Module) {
            val children = findLastChildren(root).toList()
            if(children.any())
                return children.asSequence().flatMap { fetch(it) }
        }

        return emptySequence()
    }

    private fun inBounds(module: Module): Boolean {
        val bounds = module.findOptionalComponentByInterface<ImmutableBounds>()
        return bounds != null && bounds.contains(x, y)
    }

    private fun findLastChildren(module: Module):Sequence<Module> {
        val inBounds = inBounds(module)

        if (module is ContainerModule && module.children.any()) {
            val children = module.children.flatMap { findLastChildren(it) }
            if(children.any())
                return children
        }

        if(inBounds)
            return sequenceOf(module)

        return emptySequence()
    }
}