package ru.nobirds.rx.ui.ninepatch

import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.BitmapAssetParameters
import ru.nobirds.rx.asset.NinePatchAssetParameters
import ru.nobirds.rx.asset.NinePatchFactoryAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.onChange
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.onRender
import ru.nobirds.rx.render.render
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.TransformComponent
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.ninepatch.NinePatch
import ru.nobirds.rx.utils.ninepatch.ScaledNinePatch

@DependentComponents(TransformComponent::class, BoundsComponent::class)
class NinePatchComponent(val assets: AssetCoordinator) : AbstractComponent() {

    private val transform: TransformComponent by dependency()
    private val bounds: BoundsComponent by dependency()

    val ninePatchNameProperty: Property<String?> = property(null)
    var ninePatchName:String? by ninePatchNameProperty

    private var ninePatchProperty: Property<NinePatch?> = property(null)
    private var ninePatch: NinePatch? by ninePatchProperty

    val scaledNinePathProperty: Property<ScaledNinePatch?> = property(null)
    var scaledNinePath: ScaledNinePatch? by scaledNinePathProperty

    private val invalidatedBounds = invalidated(true)

    // todo: observable implementation
    val contentSizeProperty: Property<ImmutableSize?> = property(null)
    var contentSize: ImmutableSize? by contentSizeProperty

    override fun setup() {
        invalidatedBounds.bindOneWaySignal(bounds.invalidated)
        // invalidatedBounds.bindOneWaySignal(contentSizeProperty.invalidated)

        contentSizeProperty.onChange { value, old, new ->
            new?.let { size ->
                ninePatch?.let { patch ->
                    scale(patch, size)
                }
            }
        }

        ninePatchNameProperty.onChange { value, old, new ->
            if(old != null)
                unload(old)

            if (new != null) {
                load(new)
            } else ninePatch = null
        }

        ninePatchProperty.onChange { value, old, new ->
            new?.let { patch ->
                scale(patch, contentSize)
            }
        }

        onUpdate {
            // todo: update()
        }

        onRender {
            render(it.renderer)
        }

        onEvent<LoadEvent> {
            it.complete(ninePatchName == null || ninePatch != null)
        }

    }

    private fun load(name: String) {
        assets.require(createNinePatchAssetParameters(name)) {
            ninePatch = it
        }
    }

    private fun unload(name: String) {
        assets.notRequired(createNinePatchAssetParameters(name))
    }

    private fun createNinePatchAssetParameters(name: String) =
            NinePatchAssetParameters(NinePatchFactoryAssetParameters(BitmapAssetParameters(name)))

    private fun update() {
        ninePatch?.let { patch ->
            invalidatedBounds.with {
                scale(patch, contentSize)
            }
        }
    }

    private fun scale(patch: NinePatch, contentSize: ImmutableSize?) {
        scaledNinePath = if (contentSize != null)
            patch.scaleForContent(contentSize) else patch.scaleTo(bounds)
    }

    private fun render(renderer: RenderConveyor) {
        scaledNinePath?.let { patch ->
            patch.regions.forEach {
                renderer.render(it.region, transform, it.bounds)
            }
        }
    }

}
