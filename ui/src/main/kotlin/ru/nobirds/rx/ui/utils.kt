package ru.nobirds.rx.ui

import ru.nobirds.rx.Layer
import ru.nobirds.rx.ui.viewport.ScreenWorldScaleComponent
import ru.nobirds.rx.ui.viewport.ViewportComponent
import ru.nobirds.rx.ui.viewport.ViewportScaleComponent
import ru.nobirds.rx.ui.viewport.WorldComponent


fun Layer.withScreenSize(): Layer = apply {
    attachAll(ViewportComponent::class,
            WorldComponent::class,
            ViewportScaleComponent::class,
            ScreenWorldScaleComponent::class)
}
