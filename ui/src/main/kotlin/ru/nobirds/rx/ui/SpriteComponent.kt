package ru.nobirds.rx.ui

import com.badlogic.gdx.graphics.g2d.TextureRegion
import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.TextureAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.AfterModuleAttachedToSceneEvent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.onChange
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.onRender
import ru.nobirds.rx.render.render
import ru.nobirds.rx.ui.alignment.PivotAtCenterOfBoundsComponent
import kotlin.reflect.KClass

@ChildComponents(SpriteComponent::class)
class Sprite(id:String? = null, vararg component:KClass<out Component>) : UiModule(id,  *component) {

    val sprite:SpriteComponent by dependency()

    var texture:String? by sprite.textureNameProperty

    fun withPivotAtCenter() {
        attach(PivotAtCenterOfBoundsComponent::class)
    }

}

fun ContainerModule.attachSprite(id:String? = null, textureName: String? = null,
                        vararg component:KClass<out Component>):Sprite = attachModule(Sprite(id, *component)) {
    texture = textureName
}

@DependentComponents(TransformComponent::class,
        BoundsComponent::class, PreferBoundsComponent::class)
class SpriteComponent(val assets:AssetCoordinator) : AbstractComponent() {

    private val transform:TransformComponent by dependency()
    private val bounds:BoundsComponent by dependency()
    private val prefer:PreferBoundsComponent by dependency()

    val textureNameProperty: Property<String?> = property<String?>(null) {
        onChange { property, old, new ->
            if(new != null)
                notifyChangeTexture(new)
            else if(old != null)
                requestUnloadTexture(old)
        }
    }

    var textureName:String? by textureNameProperty

    private var texture:TextureRegion? = null

    private fun updateSize(width: Int, height: Int) {
        bounds.size(width.toFloat(), height.toFloat())
        prefer.min(width.toFloat(), height.toFloat())
        prefer.pref(width.toFloat(), height.toFloat())
    }

    private fun notifyChangeTexture(texture:String) {
        if(parent?.isAttachedToScene ?: false)
            requestTexture(texture)
        else {
            onEvent<AfterModuleAttachedToSceneEvent> { requestTexture(texture) }
        }
    }

    private fun requestTexture(texture: String) {
        assets.require(TextureAssetParameters(texture)) {
            this.texture = TextureRegion(it)
            updateSize(it.width, it.height)
        }
    }

    private fun requestUnloadTexture(texture: String) {
        assets.notRequired(TextureAssetParameters(texture))
    }

    fun render(conveyor: RenderConveyor) {
        val texture = texture
        if (texture != null) {
            conveyor.render(texture, transform)
        }
    }

    override fun setup() {
        onRender {
            render(it.renderer)
        }

        onEvent<LoadEvent> {
            it.complete(texture != null)
        }
    }

}