package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.ui.BoundsComponent

@DependentComponents(BoundsComponent::class)
abstract class AbstractScaleBoundsComponent() : AbstractComponent() {

    private val bounds:BoundsComponent by dependency()

    abstract fun scale(bounds:BoundsComponent)

    abstract val changed:Observable<Signal>

    override fun setup() {
        changed.subscribe {
            scale(bounds)
        }
    }

}

