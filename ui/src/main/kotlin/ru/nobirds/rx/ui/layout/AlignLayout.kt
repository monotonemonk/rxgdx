package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.component.findOptionalComponent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.SpacingComponent
import ru.nobirds.rx.ui.UiContainerModule
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.attachModule
import ru.nobirds.rx.ui.setupComponent
import kotlin.reflect.KClass

@ChildComponents(AlignLayout::class)
class Container(id:String? = null, vararg component: KClass<out Component>) : UiContainerModule(id, *component) {

    val alignLayout:AlignLayout by dependency()

    fun align(vertical:VerticalAlign, horizontal: HorizontalAlign) {
        alignLayout.align(vertical, horizontal)
    }

}

fun ContainerModule.attachContainer(id:String? = null, vararg components: KClass<out Component>, initializer:Container.()->Unit) =
        attachModule(Container(id, *components), initializer)

class AlignLayout() : LayoutComponent() {

    val verticalAlignProperty: Property<VerticalAlign> = property(VerticalAlign.none)
    var verticalAlign: VerticalAlign by verticalAlignProperty

    val horizontalAlignProperty: Property<HorizontalAlign> = property(HorizontalAlign.none)
    var horizontalAlign: HorizontalAlign by horizontalAlignProperty

    override fun updatePreferSize() {
        preferBounds.pref(bounds.width, bounds.height)
        preferBounds.max(bounds.width, bounds.height)
    }

    override fun updateLayout() {
        for (module in childrenWithBounds) {
            val bounds = module.findComponent<BoundsComponent>()
            val margin = module.findOptionalComponent<SpacingComponent>()?.margin ?: 0f

            val x = when (horizontalAlign) {
                HorizontalAlign.left -> 0f + margin
                HorizontalAlign.right -> this.bounds.width - bounds.width - margin
                HorizontalAlign.center -> (this.bounds.width - bounds.width ) / 2f
                HorizontalAlign.none -> bounds.x
            }

            val y = when (verticalAlign) {
                VerticalAlign.bottom -> 0f + margin
                VerticalAlign.top -> this.bounds.height - bounds.height - margin
                VerticalAlign.center -> (this.bounds.height - bounds.height) / 2f
                VerticalAlign.none -> bounds.y
            }

            bounds.position(x, y)
        }
    }

    fun align(vertical:VerticalAlign, horizontal: HorizontalAlign) {
        this.verticalAlign = vertical
        this.horizontalAlign = horizontal
    }

}

fun Module.setupAlignLayout(vertical:VerticalAlign, horizontal: HorizontalAlign) = setupComponent<AlignLayout> {
    align(vertical, horizontal)
}