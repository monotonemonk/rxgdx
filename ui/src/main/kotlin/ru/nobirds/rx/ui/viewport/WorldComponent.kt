package ru.nobirds.rx.ui.viewport

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.setupComponent
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.MutableSize
import ru.nobirds.rx.property.invalidated

class WorldSizeChangedEvent(override val width:Float, override val height:Float) : AbstractEvent(), ImmutableSize

class WorldComponent() : AbstractComponent(), MutableSize, Invalidatable {

    val widthProperty: Property<Float> = property(0f)
    override var width:Float by widthProperty

    val heightProperty: Property<Float> = property(0f)
    override var height:Float by heightProperty

    override val invalidated: Observable<Signal> = changed(widthProperty, heightProperty)

    private val invalidatedWorld: InvalidatedProperty = invalidated()

    override fun setup() {
        invalidatedWorld.bindOneWaySignal(invalidated)

        onUpdate {
            invalidatedWorld.with {
                parent?.fire(WorldSizeChangedEvent(width, height),
                        EventTraverseStrategies.modulesAndComponents)
            }
        }

    }

}

fun Module.setupWorld(width: Float, height: Float):WorldComponent = setupComponent { size(width, height) }

@DependentComponents(WorldComponent::class)
class ScreenWorldScaleComponent() : AbstractComponent() {

    private val world:WorldComponent by dependency()

    override fun setup() {
        onEvent<ResizeEvent> {
            updateWorld(world, it)
        }
    }

    fun updateWorld(world: MutableSize, screen: ImmutableSize) {
        world.size(screen)
    }

}
