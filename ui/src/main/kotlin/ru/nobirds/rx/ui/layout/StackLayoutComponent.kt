package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.component.findOptionalComponent
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PreferBoundsComponent
import kotlin.sequences.forEach

class StackLayoutComponent() : LayoutComponent() {

    override fun updatePreferSize() {
        childrenWithBounds.forEach {
            val size = it.findComponent<BoundsComponent>()
            val preferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            if (preferBounds != null) {
                this.preferBounds.preferBounds(preferBounds)
            } else {
                this.preferBounds.size(size)
            }
        }
    }

    override fun updateLayout() {
        childrenWithBounds.forEach {
            val bounds = it.findComponent<BoundsComponent>()
            bounds.bounds(0f, 0f, this.bounds.width, this.bounds.height)
        }
    }

}