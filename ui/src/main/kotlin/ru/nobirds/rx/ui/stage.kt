package ru.nobirds.rx.ui

import ru.nobirds.rx.Director
import ru.nobirds.rx.Layer
import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.SceneDirector
import ru.nobirds.rx.Stage
import ru.nobirds.rx.UpdateEvent
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.module.AbstractContainerModule
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.DisposeEvent
import ru.nobirds.rx.render.RenderConveyor
import kotlin.reflect.KClass

fun SceneDirector.buildScene(name:String, builder:Stage.()->Unit) {
    addScene(name) {
        val stage = StageModule()
        stage.builder()
        stage
    }
}

fun Stage.attachLayer(name: String, vararg components: KClass<out Component>, builder:LayerModule.()->Unit):Layer {
    val layer = LayerModule(name, *components)
    layer.builder()
    attach(layer)
    return layer
}

open class StageModule() : AbstractContainerModule(), Stage {

    override val layers: Sequence<Layer>
        get() = children.filterIsInstance<Layer>()

    override var parent: ContainerModule? = null
        set(value) {
            throw UnsupportedOperationException("Impossible set parent for root")
        }

    private val updateEvent = UpdateEvent(0f)
    private val loadEvent = LoadEvent()

    override val isAttachedToScene: Boolean
        get() = true

    override val loaded: Boolean
        get() = load()

    override fun dispose() {
        fire(DisposeEvent, EventTraverseStrategies.modulesAndComponents)
    }

    override fun load(): Boolean {
        fire(loadEvent.renew(), EventTraverseStrategies.modulesAndComponents)
        return loadEvent.complete
    }

    override fun update(delta: Float) {
        fire(updateEvent.delta(delta), EventTraverseStrategies.modulesAndComponents)
    }

    override fun render(renderer: RenderConveyor) {
        for (layer in layers) {
            layer.render(renderer)
            if(Director.debug)
                layer.renderDebug(renderer)
        }
    }

}

