package ru.nobirds.rx.ui.alignment

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.ui.viewport.WorldSizeChangedEvent

@DependentComponents(PositionComponent::class)
class AlignAtCenterOfWorldComponent() : AbstractComponent() {

    private val position: PositionComponent by dependency()

    override fun setup() {
        onEvent<WorldSizeChangedEvent> {
            position.position(it.width / 2f, it.height / 2f)
        }
    }

}

@DependentComponents(PositionComponent::class)
class AlignAtCenterOfScreenComponent() : AbstractComponent() {

    private val position: PositionComponent by dependency()

    override fun setup() {
        onEvent<ResizeEvent> {
            position.position(it.width / 2f, it.height / 2f)
        }
    }

}

