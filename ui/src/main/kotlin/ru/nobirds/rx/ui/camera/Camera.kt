package ru.nobirds.rx.ui.camera

import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.attach
import ru.nobirds.rx.module.AbstractModule
import ru.nobirds.rx.ui.alignment.AlignAtCenterOfScreenComponent
import ru.nobirds.rx.ui.alignment.AlignAtCenterOfWorldComponent
import kotlin.reflect.KClass

@ChildComponents(CameraComponent::class)
class Camera(id:String? = null, vararg components: KClass<out Component>) : AbstractModule(id, *components) {

    fun atCenterOfWorld(): Camera = apply {
        attach<AlignAtCenterOfWorldComponent>()
    }

    fun atCenterOfScreen(): Camera = apply {
        attach<AlignAtCenterOfScreenComponent>()
    }

}