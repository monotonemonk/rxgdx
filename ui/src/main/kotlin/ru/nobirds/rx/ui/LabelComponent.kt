package ru.nobirds.rx.ui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.utils.Align
import ru.nobirds.rx.LoadEvent
import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.asset.AssetCoordinator
import ru.nobirds.rx.asset.FontAssetParameters
import ru.nobirds.rx.asset.FontGeneratorAssetParameters
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.onDispose
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.onRender
import ru.nobirds.rx.render.render
import kotlin.properties.Delegates
import kotlin.reflect.KClass

@ChildComponents(LabelComponent::class, SpacingComponent::class)
class Label(id:String? = null, vararg component:KClass<out Component>) : UiModule(id, *component) {

    val label:LabelComponent by dependency()

    var text:String by label.textProperty
    var font:String by label.nameProperty
    var size:Int by label.sizeProperty
    var color:Color by label.colorProperty
    var align:HorizontalAlign by label.horizontalAlignProperty


}

fun ContainerModule.attachLabel(id:String? = null, vararg components: KClass<out Component>, initializer: Label.()->Unit) =
        attachModule(Label(id, *components), initializer)

@DependentComponents(TransformComponent::class, BoundsComponent::class)
class LabelComponent(val assets:AssetCoordinator) : AbstractComponent() {

    private val transform:TransformComponent by dependency()
    private val bounds:BoundsComponent by dependency()

    val textProperty:Property<String> = property("")
    var text:String by textProperty

    val nameProperty:Property<String> = property("arial-15.fnt")
    var name:String by nameProperty

    val sizeProperty:Property<Int> = property(12)
    var size:Int by sizeProperty

    val colorProperty:Property<Color> = property(Color.WHITE )
    var color:Color by colorProperty

    val horizontalAlignProperty:Property<HorizontalAlign> = property(HorizontalAlign.left)
    var horizontalAlign:HorizontalAlign by horizontalAlignProperty

    private var font:BitmapFont? by Delegates.observable<BitmapFont?>(null) { p, o, n ->
        updateLayout()
    }

    private fun updateLayout() {
        if (font != null) {
            layout.setText(font, text, color, 0f, horizontalAlign.toGdx(), true)
            bounds.size(layout.width, layout.height)
        }
    }

    private fun HorizontalAlign.toGdx():Int = when(this) {
        HorizontalAlign.left -> Align.left
        HorizontalAlign.right -> Align.right
        HorizontalAlign.center -> Align.center
        else -> 0
    }

    private val fontChanged = changed(nameProperty, sizeProperty, colorProperty)

    private val layout:GlyphLayout = GlyphLayout()

    private val invalidatedFont: InvalidatedProperty = invalidated().apply {
        bindOneWaySignal(fontChanged)
    }

    private val invalidatedText: InvalidatedProperty = invalidated().apply {
        bindOneWaySignal(textProperty)
        bindOneWaySignal(horizontalAlignProperty)
    }

    override fun setup() {
        onUpdate {
            update()
        }

        onRender {
            render(it.renderer)
        }

        onEvent<LoadEvent> {
            it.complete(font != null)
        }

        onDispose {
            assets.notRequired(createFontAssetParameters())
        }
    }

    private fun update() {
        invalidatedText.with {
            updateLayout()
        }
        invalidatedFont.with {
            changeFont()
        }
    }

    private fun changeFont() {
        val assetParameters = createFontAssetParameters()

        assets.require(assetParameters) { this.font = it }
    }

    private fun createFontAssetParameters(): FontAssetParameters {
        val parameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
            // todo
            size = this@LabelComponent.size
            color = this@LabelComponent.color
        }

        return FontAssetParameters(FontGeneratorAssetParameters(name), parameters)
    }

    private fun render(batch: RenderConveyor) {
        if (font != null) {
            batch.render(font!!, layout, transform)
        }
    }
}