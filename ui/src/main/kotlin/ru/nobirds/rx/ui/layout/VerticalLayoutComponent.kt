package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.component.findOptionalComponent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.UiContainerModule
import ru.nobirds.rx.ui.attachModule
import kotlin.reflect.KClass

@ChildComponents(VerticalLayoutComponent::class)
class VBox(id:String? = null, vararg component:KClass<out Component>) : UiContainerModule(id, *component) {

    val verticalLayout:VerticalLayoutComponent by dependency()
    var horizontalAlign:HorizontalAlign by verticalLayout.horizontalAlignProperty

}

fun ContainerModule.attachVBox(id:String? = null, vararg components:KClass<out Component>, initializer:VBox.()->Unit):VBox
        = attachModule(VBox(id, *components), initializer)

class VerticalLayoutComponent() : LayoutComponent() {

    val horizontalAlignProperty: Property<HorizontalAlign> = property(HorizontalAlign.none)
    var horizontalAlign:HorizontalAlign by horizontalAlignProperty

    override fun updatePreferSize() {
        var width = 0f
        var height = spacing.padding.top + spacing.padding.bottom + spacing.margin

        childrenWithBounds.forEach {
            val bounds = it.findComponent<BoundsComponent>()
            val prefBounds = it.findOptionalComponent<PreferBoundsComponent>()

            width = Math.max(width, prefBounds?.prefWidth ?: bounds.width) // max width
            height += prefBounds?.prefHeight ?: bounds.height // sum of heights
        }

        width += spacing.padding.left + spacing.padding.right

        preferBounds.pref(width, height)
    }

    override fun updateLayout() {
        val innerWidth = bounds.width - spacing.padding.left - spacing.padding.right

        var y = bounds.height - spacing.padding.top + spacing.margin

        childrenWithBounds.forEach {
            val childBounds = it.findComponent<BoundsComponent>()
            val childPreferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            var width = childBounds.width
            var height = childBounds.height

            if (childPreferBounds != null) {
                width = Math.max(Math.min(childPreferBounds.prefWidth, innerWidth), childPreferBounds.minWidth)

                if(childPreferBounds.maxWidth > 0f && width > childPreferBounds.maxWidth)
                    width = childPreferBounds.maxWidth

                height = childPreferBounds.prefHeight
            }

            var x = spacing.padding.left + when(horizontalAlign) {
                HorizontalAlign.right -> innerWidth - width
                HorizontalAlign.center -> (innerWidth - width) / 2f
                else -> 0f
            }

            y -= height + spacing.margin

            childBounds.bounds(x, y, width, height)
        }

    }

}

