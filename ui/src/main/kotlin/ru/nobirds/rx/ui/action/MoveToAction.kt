package ru.nobirds.rx.ui.action

import com.badlogic.gdx.math.Interpolation
import ru.nobirds.rx.action.AbstractTemporalAction
import ru.nobirds.rx.action.ActionBuilder
import ru.nobirds.rx.action.actions
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.utils.ImmutableVec2f

class MoveToAction(val target: Module, val end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) :
        AbstractTemporalAction(duration, interpolation) {

    private val position: PositionComponent = target.findComponent()

    private val start by lazy { ImmutableVec2f.readonly(position.x, position.y) }

    override fun update(processed: Float) {
        position.position(compute(processed) { x }, compute(processed) { y })
    }

    private inline fun compute(processed: Float, component: ImmutableVec2f.()->Float):Float
            = start.component() + (end.component() - start.component()) * processed
}

fun Module.moveTo(end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    actions().run(MoveToAction(this, end, duration, interpolation))
}

fun ActionBuilder.moveTo(end: ImmutableVec2f, duration:Float, interpolation: Interpolation = Interpolation.linear) {
    val module = component.parent ?: throw IllegalStateException("Component need to be attached to module")
    action(MoveToAction(module, end, duration, interpolation))
}