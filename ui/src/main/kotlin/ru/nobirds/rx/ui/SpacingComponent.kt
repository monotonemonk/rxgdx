package ru.nobirds.rx.ui

import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.utils.MutableSpacing
import ru.nobirds.rx.utils.Spacing

class SpacingComponent() : AbstractComponent() {

    val padding: MutableSpacing = Spacing(0f, 0f, 0f, 0f)

    val marginProperty: Property<Float> = property(0f)
    var margin:Float by marginProperty // todo: migrate to 4 components

}