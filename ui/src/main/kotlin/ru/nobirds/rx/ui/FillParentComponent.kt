package ru.nobirds.rx.ui

import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.component.hasComponent
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property

@DependentComponents(BoundsComponent::class)
class FillParentComponent() : AbstractComponent(), Invalidatable {

    private val bounds:BoundsComponent by dependency()

    val fillParentWidthProperty: Property<Boolean> = property(false)
    var fillParentWidth: Boolean by fillParentWidthProperty

    val fillParentHeightProperty: Property<Boolean> = property(false)
    var fillParentHeight: Boolean by fillParentHeightProperty

    override val invalidated: Observable<Signal> = changed(fillParentWidthProperty, fillParentHeightProperty)

    private var invalidatedFillParent: InvalidatedProperty = invalidated()

    private fun invalidateFillParent() {
        if(fillParentWidth || fillParentHeight)
            invalidatedFillParent.invalidate()
    }

    override fun setup() {
        invalidatedFillParent.bindOneWaySignal(invalidated)

        onEvent<ParentSizeChangedEvent> {
            invalidateFillParent()
        }

        onUpdate {
            invalidatedFillParent.with {
                updateSize()
            }
        }
    }

    protected fun updateSize() {
        require(parent != null)

        val parent = parent!!.parent

        if ((fillParentWidth || fillParentHeight)
                && parent != null && parent.hasComponent<BoundsComponent>()) {

            val parentBounds = parent.findComponent<BoundsComponent>()

            if(fillParentWidth) {
                bounds.width = parentBounds.width
            }

            if (fillParentHeight) {
                bounds.height = parentBounds.height
            }
        }
    }

    fun fillParent(width:Boolean = true, height: Boolean = true) {
        fillParentWidth = width
        fillParentHeight = height
    }


}