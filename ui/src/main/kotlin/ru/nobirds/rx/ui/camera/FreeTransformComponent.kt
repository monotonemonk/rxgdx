package ru.nobirds.rx.ui.camera

import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.PositionComponent
import ru.nobirds.rx.ui.input.PanGestureEvent

@DependentComponents(PositionComponent::class)
class FreeTransformComponent() : AbstractComponent() {

    private val position:PositionComponent by dependency()

    val invertProperty:Property<Boolean> = property(false)
    var invert:Boolean by invertProperty

    override fun setup() {
        onEvent<PanGestureEvent> {
            update(it)
        }
    }

    private fun update(it: PanGestureEvent) {
        val factor = if (invert) -1 else 1
        position.translate(factor * -it.deltaX, factor * it.deltaY)
    }
}