package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.component.findComponent
import ru.nobirds.rx.component.findOptionalComponent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.UiContainerModule
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.attachModule
import kotlin.reflect.KClass

@ChildComponents(HorizontalLayoutComponent::class)
class HBox(id:String? = null, vararg component: KClass<out Component>) : UiContainerModule(id, *component) {

    val horizontalLayout: HorizontalLayoutComponent by dependency()

    var verticalAlign: VerticalAlign by horizontalLayout.verticalAlignProperty

}

fun ContainerModule.attachHBox(id:String? = null, vararg components:KClass<out Component>, initializer:HBox.()->Unit):HBox
        = attachModule(HBox(id, *components), initializer)

class HorizontalLayoutComponent() : LayoutComponent() {

    val verticalAlignProperty: Property<VerticalAlign> = property(VerticalAlign.none)
    var verticalAlign: VerticalAlign by verticalAlignProperty

    override fun updatePreferSize() {
        var width = spacing.padding.left + spacing.padding.right + spacing.margin
        var height = 0f

        for (it in childrenWithBounds) {
            val bounds = it.findComponent<BoundsComponent>()
            val prefBounds = it.findOptionalComponent<PreferBoundsComponent>()

            width += prefBounds?.prefWidth ?: bounds.width // sum of heights
            height = Math.max(height, prefBounds?.prefHeight ?: bounds.height) // max height
        }

        height += spacing.padding.top + spacing.padding.bottom

        preferBounds.pref(width, height)
    }

    override fun updateLayout() {
        val innerHeight = bounds.height - spacing.padding.top - spacing.padding.bottom

        var x = bounds.width - spacing.padding.left + spacing.margin

        childrenWithBounds.forEach {
            val childBounds = it.findComponent<BoundsComponent>()
            val childPreferBounds = it.findOptionalComponent<PreferBoundsComponent>()

            var width = childBounds.width
            var height = childBounds.height

            if (childPreferBounds != null) {
                width = childPreferBounds.prefWidth

                height = Math.max(Math.min(childPreferBounds.prefHeight, innerHeight), childPreferBounds.minHeight)

                if(childPreferBounds.maxHeight > 0f && height > childPreferBounds.maxHeight)
                    height = childPreferBounds.maxHeight
            }

            var y = spacing.padding.top + when(verticalAlign) {
                VerticalAlign.bottom -> innerHeight - height
                VerticalAlign.center -> (innerHeight - height) / 2f
                else -> 0f
            }

            x -= width + spacing.margin

            childBounds.bounds(x, y, width, height)
        }

    }

}