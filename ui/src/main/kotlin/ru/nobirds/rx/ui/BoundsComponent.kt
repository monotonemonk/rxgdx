package ru.nobirds.rx.ui

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.event.AbstractEvent
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.observable.filter
import ru.nobirds.rx.observable.filterIs
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.ui.debug.DebugBoundsRenderComponent
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.viewport.WorldSizeChangedEvent
import ru.nobirds.rx.utils.MutableBounds
import ru.nobirds.rx.utils.MutablePosition
import ru.nobirds.rx.utils.obtainVec2
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.square
import ru.nobirds.rx.utils.x1
import ru.nobirds.rx.utils.x2
import ru.nobirds.rx.utils.x3
import ru.nobirds.rx.utils.x4
import ru.nobirds.rx.utils.y1
import ru.nobirds.rx.utils.y2
import ru.nobirds.rx.utils.y3
import ru.nobirds.rx.utils.y4

abstract class AbstractPositionComponent() : AbstractComponent(), MutablePosition, Invalidatable {

    val xProperty: Property<Float> = property(0f)
    override var x:Float by xProperty

    val yProperty: Property<Float> = property(0f)
    override var y:Float by yProperty

    override val invalidated: Observable<Signal> = changed(xProperty, yProperty)

}

class PositionComponent() : AbstractPositionComponent() {

    override fun toString(): String = "position [$x,$y]"

}

class PivotComponent() : AbstractPositionComponent() {

    override fun toString(): String = "pivot [$x,$y]"

}

object ParentSizeChangedEvent : AbstractEvent()
object ChildSizeChangedEvent : AbstractEvent()

@DependentComponents(TransformComponent::class, DebugBoundsRenderComponent::class)
class BoundsComponent() : AbstractComponent(), MutableBounds, Invalidatable {

    val position:PositionComponent by dependency()
    val transform:TransformComponent by dependency()

    override var x: Float
        get() = position.x
        set(value) {
            position.x = value
        }

    override var y: Float
        get() = position.y
        set(value) {
            position.y = value
        }

    val widthProperty: Property<Float> = property(0f)
    override var width:Float by widthProperty

    val heightProperty: Property<Float> = property(0f)
    override var height:Float by heightProperty

    override val invalidated: Observable<Signal> = changed(widthProperty, heightProperty)

    private val invalidatedBounds: InvalidatedProperty = invalidated(true)

    val boundsEvents:Observable<AbstractScreenEvent> = events()
            .filterIs<AbstractScreenEvent>()
            .filter { filterInBounds(it) }

    private fun filterInBounds(e:AbstractScreenEvent):Boolean =
            contains(e.worldX, e.worldY)

    override fun setup() {
        invalidatedBounds.bindOneWaySignal(invalidated)
        invalidatedBounds.bindOneWaySignal(position.invalidated)

        onUpdate {
            invalidatedBounds.with {
                parent?.fire(ParentSizeChangedEvent, EventTraverseStrategies.modulesAndComponents)
                parent?.fire(ChildSizeChangedEvent, EventTraverseStrategies.parentsAndComponents)
            }
        }

    }

    override fun toString(): String = "bounds [$x x $y,$width x $height]"

    override fun contains(x:Float, y:Float): Boolean = poolable {
        val position = obtainVec2(x, y)

        val affine = transform.toAffine()

        val v1 = obtainVec2(affine.x1(), affine.y1())
        val v2 = obtainVec2(affine.x2(height), affine.y2(height))
        val v3 = obtainVec2(affine.x3(width, height), affine.y3(width, height))
        val v4 = obtainVec2(affine.x4(width), affine.y4(width))

        val trianglesSquare =
                square(v1, v2, position) +
                square(v2, v3, position) +
                square(v3, v4, position) +
                square(v4, v1, position)

        val rectangleSquare = square(v1, v2, v3, v4)

        val outside = trianglesSquare.toInt() > rectangleSquare.toInt() // todo!!

        !outside
    }

}

@DependentComponents(BoundsComponent::class)
class ScreenBoundsScaleComponent() : AbstractComponent() {

    private val bounds:BoundsComponent by dependency()

    override fun setup() {
        onEvent<ResizeEvent> {
            bounds.bounds(it)
        }
    }

}

@DependentComponents(BoundsComponent::class)
class WorldBoundsScaleComponent() : AbstractComponent() {

    private val bounds:BoundsComponent by dependency()

    override fun setup() {
        onEvent<WorldSizeChangedEvent> {
            bounds.bounds(it)
        }
    }

}
