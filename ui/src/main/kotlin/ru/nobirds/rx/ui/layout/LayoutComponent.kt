package ru.nobirds.rx.ui.layout

import ru.nobirds.rx.ResizeEvent
import ru.nobirds.rx.annotation.DependentComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.dependency
import ru.nobirds.rx.component.filterByComponent
import ru.nobirds.rx.component.findOptionalComponent
import ru.nobirds.rx.component.onAttachedToParent
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.ui.BoundsComponent
import ru.nobirds.rx.ui.ChildSizeChangedEvent
import ru.nobirds.rx.ui.PreferBoundsComponent
import ru.nobirds.rx.ui.SpacingComponent

@DependentComponents(
        BoundsComponent::class,
        PreferBoundsComponent::class,
        SpacingComponent::class)
abstract class LayoutComponent() : AbstractComponent(), Invalidatable {

    val bounds: BoundsComponent by dependency()
    val preferBounds: PreferBoundsComponent by dependency()
    val spacing: SpacingComponent by dependency()

    private val invalidatedLayout: InvalidatedProperty = invalidated()
    private val invalidatedSize: InvalidatedProperty = invalidated()

    override val invalidated: Observable<Signal> = changed(invalidatedLayout, invalidatedSize)

    val childrenWithBounds: Sequence<Module>
        get() {
            val parent = parent
            return if(parent is ContainerModule)
                parent.children.filterByComponent<BoundsComponent>()
            else
                emptySequence()
        }

    val childrenBounds:Sequence<BoundsComponent>
        get() {
            val parent = parent
            return if(parent is ContainerModule)
                parent.children.map { it.findOptionalComponent<BoundsComponent>() }.filterNotNull()
            else
                emptySequence()
        }

    override fun setup() {
        onAttachedToParent {
            invalidate()
        }

        onEvent<ChildSizeChangedEvent> {
            invalidate()
        }

        onEvent<ResizeEvent> {
            invalidate()
        }

        bounds.invalidated.subscribe {
            invalidate()
        }

        onUpdate {
            updateLayoutIfInvalidated()
        }
    }

    private fun updateLayoutIfInvalidated() {
        invalidatedSize.with {
            updatePreferSize()
            updateSize()
        }

        invalidatedLayout.with {
            updateLayout()
        }
    }

    private fun updateSize() {
        bounds.size(preferBounds.prefWidth, preferBounds.prefHeight)
    }

    fun invalidate() {
        invalidatedLayout.invalidate()
        invalidatedSize.invalidate()
    }

    protected abstract fun updatePreferSize()

    protected abstract fun updateLayout()

}

