package ru.nobirds.rx.ui

import ru.nobirds.rx.Director
import ru.nobirds.rx.Layer
import ru.nobirds.rx.NoneProjection
import ru.nobirds.rx.Projection
import ru.nobirds.rx.annotation.ChildComponents
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.Component
import ru.nobirds.rx.event.EventTraverseStrategies
import ru.nobirds.rx.event.TraverseStrategies
import ru.nobirds.rx.event.fire
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.AbstractContainerModule
import ru.nobirds.rx.module.Module
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.module.findById
import ru.nobirds.rx.onUpdate
import ru.nobirds.rx.property.InvalidatedProperty
import ru.nobirds.rx.property.ObservableValue
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.bindOneWay
import ru.nobirds.rx.property.bindOneWaySignal
import ru.nobirds.rx.property.invalidated
import ru.nobirds.rx.property.property
import ru.nobirds.rx.render.DebugRenderEvent
import ru.nobirds.rx.render.RenderConveyor
import ru.nobirds.rx.render.RenderEvent
import ru.nobirds.rx.render.render
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.input.InputTraverseStrategy
import ru.nobirds.rx.utils.obtainVec2
import ru.nobirds.rx.utils.poolable
import kotlin.reflect.KClass


@ChildComponents(ProjectionFinderComponent::class)
class LayerModule(id:String, vararg moduleComponents: KClass<out Component>) : AbstractContainerModule(id, *moduleComponents), Layer {

    val projectionFinder:ProjectionFinderComponent by dependency()

    val projectionProperty: Property<Projection> = property(NoneProjection)
    override var projection:Projection by projectionProperty

    val debugProperty: Property<Boolean> = property(true)
    override var debug:Boolean by debugProperty

    private val renderEvent = RenderEvent()
    private val renderDebugEvent = DebugRenderEvent()

    init {
        projectScreenEvents()
    }

    private fun projectScreenEvents() {
        onEvent<AbstractScreenEvent> { e ->
            projectAndBubble(e)
        }
    }

    private fun projectAndBubble(e: AbstractScreenEvent) = poolable {
        projection.unproject(obtainVec2(e.x, e.y)).apply {
            e.worldX = x
            e.worldY = y
        }

        bubbleEvent(e)
    }

    private fun bubbleEvent(e: AbstractScreenEvent) {
        e.bubbling = true
        fire(e, EventTraverseStrategies.parentsAndComponents,
                InputTraverseStrategy(e.worldX, e.worldY)) // todo: pool strategy
        e.bubbling = false
    }

    override fun render(renderer: RenderConveyor) {
        renderer.render(projection) {
            fire(renderEvent.withDeltaAndRenderer(Director.delta, renderer),
                    EventTraverseStrategies.modulesAndComponents, TraverseStrategies.ChildByChild)
        }
    }

    override fun renderDebug(renderer: RenderConveyor) {
        renderer.render(projection) {
            fire(renderDebugEvent.withDeltaAndRenderer(Director.delta, renderer),
                    EventTraverseStrategies.modulesAndComponents, TraverseStrategies.ChildByChild)
        }
    }
}

class ProjectionFinderComponent() : AbstractComponent() {

    val projectionModuleIdProperty:Property<String?> = property(null)
    var projectionModuleId:String? by projectionModuleIdProperty

    val invalidatedProjectionId: InvalidatedProperty = invalidated(false)

    val projectionProperty:Property<Projection> = property(NoneProjection)
    var projection:Projection by projectionProperty

    override fun setup() {
        invalidatedProjectionId.bindOneWaySignal(projectionModuleIdProperty.invalidated)

        onUpdate {
            updateProjection(parent, projectionModuleId)
        }
    }

    private fun updateProjection(parent: Module?, projectionModuleId: String?) {
        if (parent != null) {
            invalidatedProjectionId.with {
                if (projectionModuleId != null) {
                    val module = parent.findById(projectionModuleId, Module::class)
                    val projection = module?.components?.filterIsInstance<Projection>()?.firstOrNull()
                    this.projection = projection!!
                } else {
                    projection = NoneProjection
                }
            }
        }
    }

    fun setupProjection(id:String): ObservableValue<Projection> {
        this.projectionModuleId = id
        return projectionProperty
    }

}

fun LayerModule.setupProjection(id:String) {
    projectionProperty.bindOneWay(projectionFinder.setupProjection(id))
}

fun LayerModule.setupProjection(module:Module) {
    projection = module.components.filterIsInstance<Projection>().first()
}
