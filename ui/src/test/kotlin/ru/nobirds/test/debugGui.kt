package ru.nobirds.test

import ru.nobirds.rx.Director
import ru.nobirds.rx.Stage
import ru.nobirds.rx.event.onBubbledEvent
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.WorldBoundsScaleComponent
import ru.nobirds.rx.ui.attachCamera
import ru.nobirds.rx.ui.attachLabel
import ru.nobirds.rx.ui.attachLayer
import ru.nobirds.rx.ui.debug.FpsComponent
import ru.nobirds.rx.ui.input.TapGestureEvent
import ru.nobirds.rx.ui.layout.attachContainer
import ru.nobirds.rx.ui.setupProjection
import ru.nobirds.rx.ui.withScreenSize

fun Stage.attachDebugGuiLayer() {
    attachLayer("debug-gui-layer") {

        withScreenSize()

        val camera = attachCamera("gui-camera")
                .atCenterOfWorld()

        setupProjection(camera)

        attachContainer("debug container", WorldBoundsScaleComponent::class) {

            align(VerticalAlign.top, HorizontalAlign.right)

            attachContainer {

                bounds.size(50f, 50f)

                align(VerticalAlign.center, HorizontalAlign.center)

                attachLabel(components = FpsComponent::class) {
                    text = "0"
                    font = "OpenSans-CondLight.ttf"
                    size = 24
                }

                onBubbledEvent<TapGestureEvent> {
                    Director.debug = !Director.debug
                }
            }
        }


    }
}
