package ru.nobirds.test

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.input.GestureDetector
import ru.nobirds.rx.Director
import ru.nobirds.rx.action.actions
import ru.nobirds.rx.action.runCircle
import ru.nobirds.rx.component.AbstractComponent
import ru.nobirds.rx.component.onAttachedToParent
import ru.nobirds.rx.director
import ru.nobirds.rx.event.onBubbledEvent
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.ContainerModule
import ru.nobirds.rx.ui.Sprite
import ru.nobirds.rx.ui.action.move
import ru.nobirds.rx.ui.action.rotate
import ru.nobirds.rx.ui.action.scaleTo
import ru.nobirds.rx.ui.attachCamera
import ru.nobirds.rx.ui.attachLayer
import ru.nobirds.rx.ui.buildScene
import ru.nobirds.rx.ui.camera.FreeTransformComponent
import ru.nobirds.rx.ui.input.AbstractScreenEvent
import ru.nobirds.rx.ui.input.GesturesToEventProcessor
import ru.nobirds.rx.ui.input.InputToEventsProcessor
import ru.nobirds.rx.ui.input.TapGestureEvent
import ru.nobirds.rx.ui.setupProjection
import ru.nobirds.rx.ui.withScreenSize
import ru.nobirds.rx.utils.ImmutableVec2f
import ru.nobirds.rx.utils.Interpolations
import ru.nobirds.rx.utils.immutableVec2
import ru.nobirds.rx.utils.obtainVec3
import ru.nobirds.rx.utils.poolable
import ru.nobirds.rx.utils.random
import ru.nobirds.rx.utils.times

fun main(args: Array<String>) {
    val game = director {
        debug = true

        addInputTranslator { InputToEventsProcessor(it) }
        addInputTranslator { GestureDetector(GesturesToEventProcessor(it)) }

        buildScene("test-scene") {
            attachLayer("main-layer", ImageCopierComponent::class) {

                withScreenSize()

                setupProjection(attachCamera("camera", FreeTransformComponent::class))
            }

            attachDebugGuiLayer()
        }

        scheduleScene("test-scene")
    }

    val configuration = com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration().apply {
        vSyncEnabled = true
        fullscreen = false
        useGL30 = true
    }

    LwjglApplication(game, configuration)
}

class ImageCopierComponent() : AbstractComponent() {

    // private val transform:TransformComponent by dependency()

    private var counter = 0

    override fun setup() {
        onAttachedToParent {
            require(it.parent is ContainerModule)
        }

        onEvent<TapGestureEvent> { attachImage(it) }
    }

    private fun attachImage(event: AbstractScreenEvent) = poolable {
        event.handled = true

        val localPosition = obtainVec3(event.worldX, event.worldY, 0f) //.mul(transform.toMatrix())

        val sprite = Sprite("sprite-${counter++}", ClickToDetachComponent::class).apply {
            withPivotAtCenter()
            texture = "whiteflag.png"
            transform.position(localPosition.x, localPosition.y)
            //transform.rotate(360f.random())
        }

        (parent as ContainerModule?)?.attach(sprite)

        val time = 3f.random() + 3f

        val target = immutableVec2(Director.screen.width.random(), Director.screen.height.random())

        val actions = sprite.actions()

        actions.runCircle {
            parallel {
                sequential {
                    scaleTo(ImmutableVec2f.zero, time / 2f, Interpolations.random())
                    scaleTo(3f.random().run { ImmutableVec2f.readonly(this, this) }, time / 2f, Interpolations.random())
                }
                sequential {
                    move(target, time / 2f, Interpolations.random())
                    move(target * -1f, time / 2f, Interpolations.random())
                }
                rotate(1800f.random(), time, Interpolations.random())
            }
        }

    }
}

class ClickToDetachComponent() : AbstractComponent() {

    override fun setup() {
        onBubbledEvent<TapGestureEvent> {
            parent?.detach()
            it.handled = true
        }
    }

}

