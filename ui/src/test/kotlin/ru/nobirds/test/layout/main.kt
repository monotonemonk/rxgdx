package ru.nobirds.test.layout

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.input.GestureDetector
import ru.nobirds.rx.director
import ru.nobirds.rx.event.onEvent
import ru.nobirds.rx.module.dependency
import ru.nobirds.rx.ui.HorizontalAlign
import ru.nobirds.rx.ui.Label
import ru.nobirds.rx.ui.ScreenBoundsScaleComponent
import ru.nobirds.rx.ui.UiContainerModule
import ru.nobirds.rx.ui.VerticalAlign
import ru.nobirds.rx.ui.attachCamera
import ru.nobirds.rx.ui.attachLabel
import ru.nobirds.rx.ui.attachLayer
import ru.nobirds.rx.ui.attachModule
import ru.nobirds.rx.ui.attachSprite
import ru.nobirds.rx.ui.buildScene
import ru.nobirds.rx.ui.camera.FreeTransformComponent
import ru.nobirds.rx.ui.input.GesturesToEventProcessor
import ru.nobirds.rx.ui.input.InputToEventsProcessor
import ru.nobirds.rx.ui.input.MouseMovedEvent
import ru.nobirds.rx.ui.layout.attachContainer
import ru.nobirds.rx.ui.layout.attachHBox
import ru.nobirds.rx.ui.layout.attachVBox
import ru.nobirds.rx.ui.ninepatch.NinePatchComponent
import ru.nobirds.rx.ui.ninepatch.NinePatchLayout
import ru.nobirds.rx.ui.setupProjection
import ru.nobirds.rx.ui.withScreenSize
import ru.nobirds.rx.utils.onChangeToState
import ru.nobirds.rx.utils.stateManager
import ru.nobirds.test.attachDebugGuiLayer

fun main(args: Array<String>) {
    val game = director {
        debug = true

        addInputTranslator { InputToEventsProcessor(it) }
        addInputTranslator { GestureDetector(GesturesToEventProcessor(it)) }

        buildScene("test-scene") {
            attachLayer("main-layer") {

                withScreenSize()

                val camera = attachCamera("camera", FreeTransformComponent::class)
                        .atCenterOfWorld()

                setupProjection(camera)

                attachContainer("sprite-container", ScreenBoundsScaleComponent::class) {

                    align(VerticalAlign.top, HorizontalAlign.center)

                    attachVBox {

                        horizontalAlign = HorizontalAlign.center

                        attachModule(TestButton("label-with-bachground", "very-very long string..."))

                        attachVBox {
                            (0..8).forEach { i ->
                                attachSprite("v-sprite$i", "whiteflag.png")
                            }
                        }

                        attachHBox {
                            (0..8).forEach { i ->
                                attachSprite("h-sprite$i", "whiteflag.png")
                            }
                        }

                    }
                }


            }

            attachDebugGuiLayer()
        }

        scheduleScene("test-scene")
    }

    val configuration = LwjglApplicationConfiguration().apply {
        vSyncEnabled = true
        fullscreen = false
        useGL30 = true
    }

    LwjglApplication(game, configuration)
}

class LabelWithBackgroundModule(id:String, text:String) : UiContainerModule(id, NinePatchComponent::class, NinePatchLayout::class) {

    private val label: Label = attachLabel("sub-label") {
        this.text = text
        font = "OpenSans-CondLight.ttf"
        color = Color.BLACK
        size = 21
    }

    private val background: NinePatchComponent by dependency()

    init {
        background.ninePatchName = "test.9.png"
    }

}

class TestButton(id:String, text:String) : UiContainerModule(id, NinePatchComponent::class, NinePatchLayout::class) {

    private val label: Label = attachLabel("sub-label") {
        this.text = text
        font = "OpenSans-CondLight.ttf"
        color = Color.BLACK
        size = 33
    }

    private val background: NinePatchComponent by dependency()

    private val states = stateManager<String> {
        onChangeToState("normal") {
            setBackground("btn/btn_default_normal.9.png")
        }
        onChangeToState("pressed") {
            setBackground("btn/btn_default_pressed.9.png")
        }
        onChangeToState("selected") {
            setBackground("btn/btn_default_selected.9.png")
        }
    }



    private fun setBackground(file: String) {
        background.ninePatchName = file
    }

    init {
        states.switchTo("normal")

        onEvent<MouseMovedEvent> {
            updateState(it)
        }

    }

    private fun updateState(it: MouseMovedEvent) {
        val state = if (bounds.contains(it.worldX, it.worldY))
            "pressed"
        else
            "normal"

        states.switchTo(state)
    }

}

