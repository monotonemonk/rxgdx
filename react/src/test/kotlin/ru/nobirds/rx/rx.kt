package ru.nobirds.rx

import org.junit.Assert
import org.junit.Test
import ru.nobirds.rx.observable.ObservableConsumer
import ru.nobirds.rx.observable.ObservableConverter
import ru.nobirds.rx.observable.observableOf
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.observable.subscribeOnce
import java.util.ArrayDeque

class RxTest {

    @Test
    fun subscriberTest() {
        val observableObserver = ObservableConsumer<String>()

        val result = ArrayDeque<String>()

        val subscription = observableObserver.subscribe { result.add(it) }

        observableObserver.onNext("test")

        Assert.assertEquals(1, result.size)
        Assert.assertEquals("test", result.poll())

        subscription.unsubscribe()

        observableObserver.onNext("test2")

        Assert.assertTrue(result.isEmpty())
    }

    @Test
    fun subscriberTest2() {
        val source = ObservableConsumer<String>()

        var value1 = ""
        val converter1 = ObservableConverter<String, Int>(source) { s, o ->
            value1 = s
        }

        source.onNext("100")

        Assert.assertEquals("", value1)

        converter1.unsubscribe()

        var value3 = ""
        val converter2 = ObservableConverter<String, Int>(source) { s, o ->
            value3 = s
            o.onNext(s.toInt())
        }

        var value2 = 0
        val subscription = converter2.subscribe {
            value2 = it
        }

        source.onNext("100")

        Assert.assertEquals(100, value2)
        Assert.assertEquals("100", value3)

        subscription.unsubscribe()

        source.onNext("200")

        Assert.assertEquals(100, value2)
        Assert.assertEquals("100", value3)
    }

    @Test
    fun subscriberTest3() {
        val observable = observableOf("test1", "test2")

        var value1 = ""
        observable.subscribe {
            value1 = it
        }

        Assert.assertEquals("test2", value1)
    }

    @Test
    fun subscriberTest4() {
        val observable = observableOf("test1", "test2")

        var value1 = ""
        observable.subscribeOnce {
            value1 = it
        }

        Assert.assertEquals("test1", value1)
    }
}