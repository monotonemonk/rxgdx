package ru.nobirds.rx

import java.util.*

interface Observable<out T> {

    fun subscribe(subscriber:Subscriber<T>):Subscription

}

fun <T> Observable<T>.subscribe(onNext:(T)->Unit):Subscription = subscribe(subscriber(onNext))
fun <T, R> Observable<T>.subscribe(subscriber: Subscriber<R>, onNext:(T)->Unit):Subscription = subscribe(subscriber.withNext(onNext))
fun <T> Observable<T>.subscriber(builder:SubscriberBuilder<T>.()->Unit):Subscription = subscribe(buildSubscriber(builder))

fun <T> Observable<T>.filter(predicate:(T)->Boolean):Observable<T> = FilteredObservable(this, predicate)

internal class FilteredObservable<T>(val observable: Observable<T>, val predicate: (T) -> Boolean) : Observable<T> {

    override fun subscribe(subscriber: Subscriber<T>): Subscription = observable.subscribe(subscriber) { value ->
        try {
            if(predicate(value))
                subscriber.onNext(value)
        } catch(e:Throwable) {
            subscriber.onError(e)
        }
    }

}

fun <T, R> Observable<T>.map(transformer:(T)->R):Observable<R> = TransformableObservable(this, transformer)

internal class TransformableObservable<T, R>(val observable: Observable<T>, val transformer: (T) -> R) : Observable<R> {

    override fun subscribe(subscriber: Subscriber<R>): Subscription = observable.subscribe(subscriber) { value ->
        try {
            subscriber.onNext(transformer(value))
        } catch(e: Throwable) {
            subscriber.onError(e)
        }
    }

}

fun <T, R> Observable<T>.flatMap(fetcher:(T)->Observable<R>):Observable<R> = FlattenObservable(this, fetcher)

internal class FlattenObservable<T, R>(val observable: Observable<T>, val fetcher: (T) -> Observable<R>) : Observable<R> {

    override fun subscribe(subscriber: Subscriber<R>): Subscription {
        val result = complexSubscription()

        result.add(observable.subscribe(subscriber) { value ->
            try {
                result.add(fetcher(value).subscriber {
                    next { subscriber.onNext(it) }
                    error { subscriber.onError(it) }
                })
            } catch(e: Throwable) {
                subscriber.onError(e)
            }
        })

        return result
    }

}

fun <T> observable(vararg values:T):Observable<T> = PredefinedSequenceObservable(values.asIterable())

internal class PredefinedSequenceObservable<T>(val values:Iterable<T>) : Observable<T> {
    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        for (value in values) {
            subscriber.onNext(value)
        }

        subscriber.onSuccess()

        return Subscription.nothing
    }
}

interface Subscription {

    companion object {
        val nothing = object : Subscription {
            override fun cancel() {
            }
        }
    }

    fun cancel()

}

fun subscription(action:()->Unit):Subscription = object : Subscription {
    override fun cancel() = action()
}

fun complexSubscription(vararg subscription: Subscription):ComplexSubscription = ComplexSubscription().apply {
    subscription.forEach {
        this@apply.add(it)
    }
}

class ComplexSubscription() : Subscription {

    private val subscriptions = ArrayList<Subscription>()

    override fun cancel() {
        subscriptions.forEach(Subscription::cancel)
    }

    fun add(subscription: Subscription) {
        this.subscriptions.add(subscription)
    }

}

interface Subscriber<in T> {

    fun onNext(value:T)

    fun onSuccess() {
    }

    fun onError(exception:Throwable) {
        throw exception
    }

}

fun <T> subscriber(onNext: (T) -> Unit):Subscriber<T> = object : Subscriber<T> {
    override fun onNext(value: T) = onNext(value)
}

fun <T> buildSubscriber(builder:SubscriberBuilder<T>.()->Unit):Subscriber<T> = SubscriberBuilder<T>().apply(builder).build()

fun <T, R> Subscriber<T>.withNext(onNext:(R)->Unit):Subscriber<R> = OnNextSubscriber(this, onNext)

internal class OnNextSubscriber<T, R>(val subscriber: Subscriber<T>, val handler: (R) -> Unit) : Subscriber<R> {
    override fun onSuccess() {
        subscriber.onSuccess()
    }

    override fun onError(exception: Throwable) {
        subscriber.onError(exception)
    }

    override fun onNext(value: R) = handler(value)
}

fun <T> Subscriber<T>.withSuccess(onSuccess: () -> Unit):Subscriber<T> = OnSuccessSubscriber(this, onSuccess)

internal class OnSuccessSubscriber<T>(subscriber: Subscriber<T>, val handler: () -> Unit) : Subscriber<T> by subscriber {
    override fun onSuccess() = handler()
}

fun <T> Subscriber<T>.withError(onError: (Throwable) -> Unit):Subscriber<T> = OnErrorSubscriber(this, onError)

internal class OnErrorSubscriber<T>(subscriber: Subscriber<T>, val handler: (Throwable) -> Unit) : Subscriber<T> by subscriber {
    override fun onError(exception: Throwable) = handler(exception)
}

class SubscriberBuilder<T> {

    private var onNext:(T)->Unit = {}
    private var onSuccess:()->Unit = {}
    private var onError:(Throwable)->Unit = { throw it }

    fun next(onNext:(T)->Unit):SubscriberBuilder<T> = apply {
        this.onNext = onNext
    }

    fun success(onSuccess:()->Unit):SubscriberBuilder<T> = apply {
        this.onSuccess = onSuccess
    }

    fun error(onError:(Throwable)->Unit):SubscriberBuilder<T> = apply {
        this.onError = onError
    }

    fun build():Subscriber<T> = object : Subscriber<T> {
        override fun onNext(value: T) = this@SubscriberBuilder.onNext(value)
        override fun onSuccess() = this@SubscriberBuilder.onSuccess()
        override fun onError(exception: Throwable) = this@SubscriberBuilder.onError(exception)
    }

}

fun main(args: Array<String>) {
    observable(1, 2, 3, 4, 5)
            .map { it * 10 }
            .filter { it % 2 == 0 }
            .flatMap { observable(it * 2, it * 3, it * 4) }
            .subscriber {
                next(::println)
                success {
                    println("Success!11")
                }
                error {
                    it.printStackTrace()
                }
            }
}