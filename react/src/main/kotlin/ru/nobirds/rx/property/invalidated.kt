package ru.nobirds.rx.property

class InvalidatedProperty(initial:Boolean = true) : Property<Boolean>(initial) {

    fun invalidate() {
        if(!value)
            this.value = true
    }

    fun makeValid() {
        if(value)
            this.value = false
    }

    inline fun with(block:()->Unit) {
        if (this.value) {
            block()
            makeValid()
        }
    }

}

fun invalidated(initial:Boolean = true): InvalidatedProperty = InvalidatedProperty(initial)