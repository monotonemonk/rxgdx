package ru.nobirds.rx.property

operator fun ObservableValue<Int>.plus(other:ObservableValue<Int>):ObservableValue<Int> = this.operate(other) { first, second -> first + second }
operator fun ObservableValue<Int>.minus(other:ObservableValue<Int>):ObservableValue<Int> = this.operate(other) { first, second -> first - second }
operator fun ObservableValue<Int>.times(other:ObservableValue<Int>):ObservableValue<Int> = this.operate(other) { first, second -> first * second }
operator fun ObservableValue<Int>.div(other:ObservableValue<Int>):ObservableValue<Int> = this.operate(other) { first, second -> first / second }