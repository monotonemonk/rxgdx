package ru.nobirds.rx.property

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.ObservableConsumer
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.observable.observable
import ru.nobirds.rx.observable.subscribe
import kotlin.collections.map

interface Binding<T> : ObservableValue<T> {

    val dependencies: Observable<Signal>

}

fun <T> binding(dependencies: Observable<Signal>, computer:()->T):Binding<T> = BindingImpl(dependencies, computer)
fun <T> binding(vararg dependencies: ObservableValue<*>, computer:()->T):Binding<T> = binding(observable(dependencies.map { it.invalidated }), computer)

fun <T, V, R> ObservableValue<T>.operate(other:ObservableValue<V>, mapper:(T, V)->R):ObservableValue<R>
        = binding(this, other) { mapper(this.value, other.value) }

fun <T, R> ObservableValue<T>.map(mapper:(T)->R):ObservableValue<R>
        = binding(this) { mapper(this.value) }

class BindingImpl<T>(override val dependencies: Observable<Signal>, val computer:()->T) : Binding<T> {

    private val changedObservable = ObservableConsumer<ValueChangedEvent<T>>()

    override var value: T = computer()
        private set

    private fun computeValue() {
        val oldValue = value
        val newValue = computer()

        if (newValue != oldValue) {
            this.value = newValue
            changedObservable.onNext(ValueChangedEvent(this, oldValue, newValue))
        }
    }

    init {
        dependencies.subscribe {
            computeValue()
        }
    }

    override val changed: Observable<ValueChangedEvent<T>>
        get() = changedObservable

}

fun MutableObservableValue<Boolean>.bindOneWaySignal(value: ObservableValue<out Any>):Subscription = bindOneWay(value, Converters.signal)

fun MutableObservableValue<Boolean>.bindOneWaySignal(observable: Observable<Signal>, value:Boolean = true):Subscription {
    return observable.subscribe {
        this@bindOneWaySignal.value = value
    }
}

