package ru.nobirds.rx.property

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.ObservableConsumer
import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

open class Property<T>(value:T) :
        MutableObservableValue<T>,
        ReadWriteProperty<Any, T> {

    private val changedObservable = ObservableConsumer<ValueChangedEvent<T>>()

    override val changed: Observable<ValueChangedEvent<T>>
        get() = changedObservable

    override var value:T by Delegates.observable(value) { p, o, n ->
        if(o != n) {
            changedObservable.onNext(ValueChangedEvent(this, o, n))
        }
    }

    override fun toString(): String = value?.toString() ?: "null"

    override fun getValue(thisRef: Any, property: KProperty<*>): T = this.value
    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) { this.value = value }

}

fun <T>  property(value:T): Property<T> = Property(value)
fun <T>  property(value:T, initializer: Property<T>.()->Unit): Property<T> = Property(value).apply(initializer)

