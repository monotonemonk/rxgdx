package ru.nobirds.rx.property

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.observable.Subscription
import ru.nobirds.rx.observable.asSignal
import ru.nobirds.rx.observable.observable
import ru.nobirds.rx.observable.subscribe
import ru.nobirds.rx.observable.subscription
import kotlin.reflect.KProperty

interface Invalidatable {

    val invalidated: Observable<Signal>

}

class ValueChangedEvent<T>(val property: ObservableValue<T>, val old:T?, val new:T)

interface ObservableValue<T> : Invalidatable {

    val value:T

    val changed: Observable<ValueChangedEvent<T>>

    override val invalidated: Observable<Signal>
        get() = changed.asSignal()

}

operator fun <T> ObservableValue<T>.getValue(thisRef: Any, property: KProperty<*>): T = value

fun <T> ObservableValue<T>.onChange(handler:(ObservableValue<T>, T?, T)->Unit): Subscription
        = changed.subscribe { handler(it.property, it.old, it.new) }

interface MutableObservableValue<T> : ObservableValue<T> {

    override var value:T

}

fun <T> MutableObservableValue<T>.bindOneWay(observableValue:ObservableValue<T>): Subscription
        = bindOneWay(observableValue, TwoWayConverters.identity())

fun <T, R> MutableObservableValue<T>.bindOneWay(observableValue:ObservableValue<R>, converter: Converter<R, T>): Subscription {
    value = converter.convert(observableValue.value)
    return observableValue.onChange { p, old, new -> value = converter.convert(new) }
}

fun <T> MutableObservableValue<T>.bindTwoWay(mutableObservableValue: MutableObservableValue<T>): Subscription
        = subscription(bindOneWay(mutableObservableValue), mutableObservableValue.bindOneWay(this))

fun <T, R> MutableObservableValue<T>.bindTwoWay(mutableObservableValue: MutableObservableValue<R>, converter: TwoWayConverter<R, T>): Subscription
        = subscription(bindOneWay(mutableObservableValue, converter), mutableObservableValue.bindOneWay(this, converter.back()))

fun changed(vararg observableValues: Invalidatable): Observable<Signal> = observable(observableValues.map { it.invalidated })
