package ru.nobirds.rx.observable

object Signal

fun Observable<*>.asSignal():Observable<Signal> = map { Signal }

fun Subscriber<Signal>.signal() {
    onNext(Signal)
}

operator fun Observable<Signal>.plus(other:Observable<Signal>):Observable<Signal> = observable(this, other)
