package ru.nobirds.rx.observable

import kotlin.collections.asIterable
import kotlin.collections.forEach

interface Subscription {

    fun unsubscribe()

}

fun subscription(unsubscriber:(Subscription)->Unit): Subscription = object: Subscription {
    override fun unsubscribe() {
        unsubscriber(this)
    }
}

fun subscription(vararg subscriptions: Subscription): Subscription = subscription(subscriptions.asIterable())

fun subscription(subscriptions: Iterable<Subscription>): Subscription = subscription {
    subscriptions.forEach { it.unsubscribe() }
}

fun emptySubscription():Subscription = Subscriptions.empty

object Subscriptions {
    val empty:Subscription = object: Subscription {
        override fun unsubscribe() {
        }
    }
}