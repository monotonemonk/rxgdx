package ru.nobirds.rx.observable

fun <T> subscriber(onNext:(T)->Unit): Subscriber<T> = object: Subscriber<T> {
    override fun onNext(value: T) {
        onNext(value)
    }
}

fun <T> subscriber(runner: Runner, onNext:(T)->Unit): Subscriber<T> = SubscriberWithRunnerWrapper<T>(subscriber(onNext), runner)

interface RunnableSupport {

    val runner: Runner
        get() = Runners.immediate

}

interface Subscriber<in T> {

    fun onNext(value:T)

}

class SubscriberWithRunnerWrapper<T>(val subscriber: Subscriber<T>, override val runner: Runner) : Subscriber<T>, RunnableSupport {
    override fun onNext(value: T) {
        if (runner == Runners.immediate) {
            subscriber.onNext(value)
        } else {
            runner.run { subscriber.onNext(value) }
        }
    }
}