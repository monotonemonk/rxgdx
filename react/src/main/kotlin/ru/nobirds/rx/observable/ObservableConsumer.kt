package ru.nobirds.rx.observable

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CopyOnWriteArraySet

open class ObservableConsumer<T>() : Observable<T>, Subscriber<T> {

    protected val subscribers by lazy { CopyOnWriteArrayList<Subscriber<T>>() }
    protected val subscriptions by lazy { CopyOnWriteArraySet<Subscription>() }

    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        this.subscribers.add(subscriber)

        val subscription = subscription {
            subscribers.remove(subscriber)
            subscriptions.remove(it)

            if(subscribers.isEmpty())
                onNoSubscribers()
        }

        subscriptions.add(subscription)

        onSubscribe(subscriber)

        return subscription
    }

    open protected fun onSubscribe(subscriber: Subscriber<T>) {
    }

    open protected fun onNoSubscribers() {
    }

    fun complete() {
        subscriptions.forEach { it.unsubscribe() }
    }

    override fun onNext(value: T) {
        subscribers.forEach { it.onNext(value) }
    }

}
