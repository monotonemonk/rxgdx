package ru.nobirds.rx.observable

class ObservableConverter<T, R>(val source: Observable<T>, override val runner: Runner = Runners.immediate, val convert:(T, ObservableConverter<T, R>)->Unit) :
        ObservableConsumer<R>(), RunnableSupport, Subscription {

    /**
     * Defer initialization, see [onSubscribe]
     **/
    private var subscription:Subscription? = null

    override fun unsubscribe() {
        subscription?.unsubscribe()
        subscription = null
    }

    /**
     * *Defer subscription* on first subscriber.
     * It allow works predefined observable values defined on onSubscribeHandler.
     **/
    override fun onSubscribe(subscriber: Subscriber<R>) {
        if(this.subscription == null)
            this.subscription  = source.subscribe(runner) {
                convert(it, this)
            }
    }

    override fun onNoSubscribers() {
        unsubscribe()
    }

    // todo: make it isolated
    fun registerSubscription(subscription: Subscription) {
        this.subscriptions.add(subscription)
    }

}