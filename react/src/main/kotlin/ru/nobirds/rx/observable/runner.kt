package ru.nobirds.rx.observable

import java.util.concurrent.Executor
import java.util.concurrent.Executors

interface Runner {

    fun run(block:()->Unit)

}

fun runner(impl:(()->Unit)->Unit):Runner = object: Runner {
    override fun run(block: () -> Unit) {
        impl(block)
    }
}


object Runners {

    val immediate:Runner = runner { it() }

    fun newThread(executor: Executor = Executors.newSingleThreadExecutor()):Runner = runner { t -> executor.execute(t) }

}

class RunnerObservableWrapper<T>(val observable: Observable<T>, override val runner: Runner) : Observable<T> by observable, RunnableSupport