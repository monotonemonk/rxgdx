package ru.nobirds.rx.observable

interface Observable<T> {

    fun subscribe(subscriber:Subscriber<T>):Subscription

}

fun <T> Observable<T>.subscribe(onNext: (T) -> Unit):Subscription = subscribe(subscriber(onNext))

fun <T> Observable<T>.subscribeOnce(onNext: (T) -> Unit):Subscription {
    var subscription:Subscription? = null

    subscription = subscribe {
        onNext(it)
        subscription?.unsubscribe()
    }

    return subscription
}

fun <T> Observable<T>.subscribe(runner: Runner, onNext: (T) -> Unit):Subscription = subscribe(subscriber(runner, onNext))

internal fun <T, R> Observable<T>.convert(
        runner: Runner = if(this is RunnableSupport) this.runner else Runners.immediate,
        convert:(T, ObservableConverter<T, R>)->Unit):Observable<R> = ObservableConverter(this, runner, convert)

fun <T, R> Observable<T>.map(mapper:(T)->R):Observable<R> =
        convert { value, consumer -> consumer.onNext(mapper(value)) }

fun <T> Observable<T>.filter(condition:(T)->Boolean):Observable<T> =
        convert { value, consumer -> if(condition(value)) consumer.onNext(value) }

fun <T, R> Observable<T>.flatMap(mapper:(T)->Observable<R>):Observable<R> = convert { value, subscriber ->
    val observable = mapper(value)
    val subscription = observable.subscribe(subscriber)
    subscriber.registerSubscription(subscription)
}

fun <T> Observable<T>.withRunner(runner: Runner):Observable<T> = RunnerObservableWrapper(this, runner)

fun <T> Observable<T>.cached():Observable<T> = CachedObservableWrapper(this)

inline fun <reified R> Observable<*>.filterIs():Observable<R> = filter { it is R }.map { it as R }

object Observables {
    val empty:Observable<*> = observable<Any?> { Subscriptions.empty }
}

fun <T> emptyObservable():Observable<T> = Observables.empty as Observable<T>

fun <T> observableOf(vararg values:T):Observable<T> = ObservableConsumer<T>().let { s ->
    val result = s.cached()
    values.forEach { s.onNext(it) }
    result
}

fun <T> observable(vararg observables: Observable<T>):Observable<T> = observable(observables.asIterable())

fun <T> observable(observables: Iterable<Observable<T>>):Observable<T> = observable { s ->
    subscription(observables.map { it.subscribe(s) })
}

fun <T> observable(subscribe:(Subscriber<T>)->Subscription):Observable<T> = object: Observable<T> {
    override fun subscribe(subscriber: Subscriber<T>): Subscription = subscribe(subscriber)
}

fun <T> Iterable<Observable<T>>.asObservable():Observable<T> = observable(this)
fun <T> Array<Observable<T>>.asObservable():Observable<T> = this.asIterable().asObservable()
fun <T> Sequence<Observable<T>>.asObservable():Observable<T> = this.asIterable().asObservable()
