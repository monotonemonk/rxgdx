package ru.nobirds.rx.observable

import java.util.concurrent.CopyOnWriteArrayList

internal class CachedObservableWrapper<T>(val observable: Observable<T>) : Observable<T> {

    private val consumer = ObservableConsumer<T>()

    private val cached = CopyOnWriteArrayList<T>()

    init {
        observable.subscribe {
            cached.add(it)
            consumer.onNext(it)
        }
    }

    override fun subscribe(subscriber: Subscriber<T>): Subscription {
        if (cached.isNotEmpty()) {
            for (item in cached) {
                subscriber.onNext(item)
            }
        }

        return consumer.subscribe(subscriber)
    }

}