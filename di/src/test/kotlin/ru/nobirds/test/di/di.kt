package ru.nobirds.test.di

import org.junit.Test
import ru.nobirds.rx.di.SimpleMutableContext

interface I
interface W

open class A : I
class B : A(), W



class DiTest {

    @Test
    fun test1() {
        val context = SimpleMutableContext()

        val originalA = A()
        val originalB = B()

        val originalSet = setOf(originalA, originalB)

        context.register("originalA", originalA)
        context.register(originalB)

        val b = context.find(B::class)

        org.junit.Assert.assertEquals(originalB, b)

        val a = context.find(A::class, "originalA")

        org.junit.Assert.assertEquals(originalA, a)

        val foundA = context.findAll(A::class).toSet()

        org.junit.Assert.assertEquals(originalSet, foundA)

        val foundI = context.findAll(I::class).toSet()

        org.junit.Assert.assertEquals(originalSet, foundI)

        val foundW = context.findAll(W::class).toSet()

        org.junit.Assert.assertEquals(setOf(originalB), foundW)
    }

}