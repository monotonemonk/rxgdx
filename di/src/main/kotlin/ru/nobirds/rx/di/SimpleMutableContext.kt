package ru.nobirds.rx.di

import java.util.HashMap
import java.util.HashSet
import kotlin.reflect.KClass

interface NameGenerator {

    fun generate(instance: Any):String

}

object ClassNameGenerator : NameGenerator {
    override fun generate(instance: Any): String = instance.javaClass.name
}

class SimpleMutableContext() : MutableContext {

    private val nameGenerator: NameGenerator = ClassNameGenerator

    private val instancesByName = HashMap<String, Any>()
    private val instancesByType = HashMap<KClass<*>, MutableSet<Any>>()
    private val instanceNames = HashMap<Any, String>()

    override fun register(instance: Any) {
        register(nameGenerator.generate(instance), instance)
    }

    override fun register(name: String, instance: Any) {
        if(instancesByName.containsKey(name))
            throw IllegalArgumentException("Instance with name $name already registered.")

        instancesByName.put(name, instance)
        instanceNames.put(instance, name)

        val types = instance.javaClass.fetchInstanceTypes().toList()

        types.forEach {
            instancesByType.getOrPut(it.kotlin) { HashSet<Any>() }.add(instance)
        }
    }

    override fun <T : Any> findAll(type: KClass<T>): Sequence<T> {
        return instancesByType.get(type)?.asSequence()?.map { it as T } ?: emptySequence()
    }

    override fun <T : Any> find(type: KClass<T>): T {
        val instances = findAll(type).toList()

        if (instances.size != 1) {
            throw IllegalStateException("Found ${instances.size} instances of type $type, but expected only one")
        }

        return instances.first()
    }

    override fun <T : Any> find(type: KClass<T>, name: String): T {
        val instance = instancesByName[name] ?: throw IllegalStateException("Instance with name $name not found.")

        if (!type.java.isAssignableFrom(instance.javaClass)) {
            throw IllegalStateException("Instance with name $name not instance of $type.")
        }

        return instance as T
    }

    override fun <T : Any> findWithPreferName(type: KClass<T>, name: String): T {
        val instances = instancesByType[type] ?: throw IllegalStateException("Instances with type $type not found.")

        if(instances.size == 1)
            return instances.first() as T

        val found = instances.firstOrNull { instanceNames[it] == name }

        if(found != null)
            return found  as T

        throw IllegalArgumentException("Instance with name $name not found")
    }
}

