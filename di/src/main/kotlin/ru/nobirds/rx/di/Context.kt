package ru.nobirds.rx.di

import kotlin.reflect.KClass

interface Context {

    fun <T:Any> findAll(type:KClass<T>):Sequence<T>

    fun <T:Any> find(type:KClass<T>):T

    fun <T:Any> find(type:KClass<T>, name:String):T

    fun <T:Any> findWithPreferName(type:KClass<T>, name:String):T

}

fun <T:Any> Context.createWithConstructorInjection(type: KClass<T>):T {
    val constructor = type.constructors.firstOrNull { it.parameters.all { it.isOptional } }

    if(constructor != null)
        return constructor.call()

    val constructors = type.java.constructors

    if(constructors.size != 1)
        throw IllegalArgumentException("Component of type ${type.qualifiedName} " +
                "have ${constructors.size} constructors, but for autowiring needs only one.")

    val constructorForAutowire = constructors.first()

    // todo: by parameter name
    val parameters = constructorForAutowire.parameterTypes.map { find(it.kotlin) }.toTypedArray()

    return constructorForAutowire.newInstance(*parameters) as T
}