package ru.nobirds.rx.di

fun Class<*>.fetchInstanceTypes():Sequence<Class<*>> =
        hierarchy().flatMap { it.interfaces.asSequence().flatMap { it.fetchInstanceTypes() + it } + it }.toSet().asSequence()

fun Class<*>.hierarchy():Sequence<Class<*>> {
    return if(this == Any::class.java) emptySequence()
    else generateSequence(this) { if(it.superclass == Any::class.java) null else it.superclass }
}
