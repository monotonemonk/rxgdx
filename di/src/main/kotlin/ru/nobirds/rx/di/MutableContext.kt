package ru.nobirds.rx.di

interface MutableContext : Context {

    fun register(instance:Any)

    fun register(name: String, instance:Any)

}