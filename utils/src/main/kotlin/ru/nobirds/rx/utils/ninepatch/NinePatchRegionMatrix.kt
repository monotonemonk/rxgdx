package ru.nobirds.rx.utils.ninepatch

import com.badlogic.gdx.graphics.g2d.TextureRegion

internal class NinePatchRegionMatrix(val texture: TextureRegion, val segments: NinePatchSegments) {

    val width:Int = segments.columnSegments.segments.size
    val height:Int = segments.rowSegments.segments.size

    private val container = createContainer()

    private fun createContainer(): Array<Array<TextureRegion>> = Array(width) { dx ->
        val (typeX, indexX, x, sizeX) = segments.columnSegments.segments[dx]

        Array(height) { dy ->
            val (typeY, indexY, y, sizeY) = segments.rowSegments.segments[dy]

            TextureRegion(texture, x, y, sizeX, sizeY)
        }
    }

    operator fun get(x: Int, y: Int): TextureRegion {
        return container[x][y]
    }

}