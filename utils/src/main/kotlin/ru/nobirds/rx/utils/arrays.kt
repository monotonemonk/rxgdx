package ru.nobirds.rx.utils

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

fun Color.toArray():FloatArray = floatArrayOf(r, g, b, a)

fun Vector2.toArray():FloatArray = floatArrayOf(x, y)
fun Vector3.toArray():FloatArray = floatArrayOf(x, y, z)

val Matrix4.array:FloatArray
    get() = `val`