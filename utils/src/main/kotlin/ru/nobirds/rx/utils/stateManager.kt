package ru.nobirds.rx.utils

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.ObservableConsumer
import ru.nobirds.rx.observable.subscribe
import java.util.HashMap

fun <S:Any> stateManager(builder: StateManager<S>.()->Unit): StateManager<S> = StateManager<S>().apply(builder)

class StateManager<S:Any> {

    private class Rules<S> {

        val fromAny: ObservableConsumer<S> by lazy { ObservableConsumer<S>() }
        val fromState:MutableMap<S, ObservableConsumer<S>> by lazy { HashMap<S, ObservableConsumer<S>>() }

    }

    private val toStates = HashMap<S, Rules<S>>()

    val state:S?
        get() = currentState

    private var currentState:S? = null

    fun toState(state:S): Observable<S> = toStates.getOrPut(state) { Rules() }.fromAny

    fun toState(from:S, to:S): Observable<S> = toStates.getOrPut(to) { Rules() }.fromState.getOrPut(from) { ObservableConsumer() }

    fun switchTo(state:S) {
        if(currentState == state)
            return

        val consumer = findStateEdge(currentState, state) ?:
                throw IllegalStateException("No way to mutate from $currentState to $state.")

        currentState = state

        consumer.onNext(state)
    }

    private fun findStateEdge(from: S?, to: S): ObservableConsumer<S>? {
        val rules = toStates[to] ?: return null

        return if(from != null)
            rules.fromState[from] ?: rules.fromAny
        else
            rules.fromAny
    }

}

inline fun <S:Any> StateManager<S>.onChangeToState(state:S, crossinline handler:()->Unit): Observable<S> {
    val edge = toState(state)

    edge.subscribe { handler() }

    return edge
}

inline fun <S:Any> StateManager<S>.onChangeToState(from:S, to:S, crossinline handler:()->Unit): Observable<S> {
    val edge = toState(from, to)

    edge.subscribe { handler() }

    return edge
}
