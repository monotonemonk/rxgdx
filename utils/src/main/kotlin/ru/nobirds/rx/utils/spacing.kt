package ru.nobirds.rx.utils

interface ImmutableSpacing {

    val top:Float
    val bottom:Float
    val right:Float
    val left:Float

}

interface MutableSpacing : ImmutableSpacing {

    override var top:Float
    override var bottom:Float
    override var right:Float
    override var left:Float

}

fun MutableSpacing.set(value:Float) {
    this.top = value
    this.bottom = value
    this.left = value
    this.right = value
}

fun MutableSpacing.set(top:Float, right: Float, bottom: Float, left: Float) {
    this.top = top
    this.bottom = bottom
    this.left = left
    this.right = right
}

data class Spacing(override var top:Float,
                   override var bottom:Float,
                   override var right:Float,
                   override var left:Float) : MutableSpacing

