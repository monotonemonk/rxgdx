package ru.nobirds.rx.utils


interface ImmutableBounds : ImmutablePosition, ImmutableSize {

    fun contains(x:Float, y:Float): Boolean = x > this.x &&
            y > this.y &&
            x < this.x + this.width &&
            y < this.y + this.height

    companion object {
        fun readonly(x:Float, y: Float, width: Float, height: Float): ImmutableBounds = ReadonlyBounds(x, y, width, height)
        fun readonly(width: Float, height: Float): ImmutableBounds = ReadonlyBounds(0f, 0f, width, height)
        fun readonly(size: ImmutableSize): ImmutableBounds = readonly(size.width, size.height)
        fun readonly(bounds: ImmutableBounds): ImmutableBounds = ReadonlyBounds(bounds.x, bounds.y, bounds.width, bounds.height)
    }

}

interface MutableBounds : MutableSize, MutablePosition, ImmutableBounds {

    fun bounds(x: Float, y: Float, width: Float, height: Float): MutableBounds = apply {
        position(x, y)
        size(width, height)
    }

    fun bounds(bounds: ImmutableBounds): MutableBounds = bounds(bounds.x, bounds.y, bounds.width, bounds.height)

    fun bounds(size: ImmutableSize): MutableBounds = bounds(0f, 0f, size.width, size.height)

    fun copyBounds(): MutableBounds = MutableBounds.zero().bounds(this)

    companion object : PoolFactory<MutableBounds> {

        override fun create(): MutableBounds = zero()

        fun bounds(x: Float, y: Float, width: Float, height: Float): MutableBounds = Bounds(x, y, width, height)
        fun zero(): MutableBounds = bounds(0f, 0f, 0f, 0f)
    }

}

internal data class ReadonlyBounds(
        override val x:Float,
        override val y:Float,
        override val width:Float,
        override val height:Float) : ImmutableBounds

internal data class Bounds(
        override var x:Float,
        override var y:Float,
        override var width:Float,
        override var height:Float) : MutableBounds
