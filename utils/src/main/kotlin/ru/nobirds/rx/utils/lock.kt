package ru.nobirds.rx.utils

inline fun <T> ifChanged(old: T, new:T, block: () -> Unit):Boolean {
    val changed = old != new
    if (changed) block()
    return changed
}
