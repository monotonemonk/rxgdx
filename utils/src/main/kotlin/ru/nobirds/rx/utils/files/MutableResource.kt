package ru.nobirds.rx.utils.files

import java.io.OutputStream

interface MutableResource {

    fun toOutputStream(append:Boolean = false): OutputStream

}