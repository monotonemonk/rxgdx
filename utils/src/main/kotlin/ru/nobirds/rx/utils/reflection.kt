package ru.nobirds.rx.utils

import kotlin.reflect.KClass

inline fun <I:Any, reified T:Any> Sequence<I>.filterIsJavaInstance():Sequence<T> = filter { T::class.java.isAssignableFrom(it.javaClass) }.map { it as T }

infix fun <T:Any, C:Any> T.instanceOf(type: KClass<C>):Boolean {
    return type.java.isAssignableFrom(this.javaClass)
}
