package ru.nobirds.rx.utils.files

import com.badlogic.gdx.files.FileHandle
import java.io.InputStream

interface ImmutableResource {

    fun toInputStream(): InputStream

    fun toFileHandle(): FileHandle

}

fun ImmutableResource.readText():String = toInputStream().reader().use { it.readText() }
fun ImmutableResource.readBytes():ByteArray = toInputStream().use { it.readBytes() }
