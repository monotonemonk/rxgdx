package ru.nobirds.rx.utils.observable

import com.badlogic.gdx.Gdx
import ru.nobirds.rx.observable.Runner
import ru.nobirds.rx.observable.runner

object GdxRunner {

    val runner: Runner = runner { t -> Gdx.app.postRunnable(t) }

}