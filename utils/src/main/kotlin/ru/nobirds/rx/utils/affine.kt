package ru.nobirds.rx.utils

import com.badlogic.gdx.math.Affine2

fun Affine2.x1() = m02
fun Affine2.y1() = m12
fun Affine2.x2(height:Float) = m01 * height + m02
fun Affine2.y2(height:Float) = m11 * height + m12
fun Affine2.x3(width:Float, height:Float) = m00 * width + m01 * height + m02
fun Affine2.y3(width:Float, height:Float) = m10 * width + m11 * height + m12
fun Affine2.x4(width:Float) = m00 * width + m02
fun Affine2.y4(width:Float) = m10 * width + m12

