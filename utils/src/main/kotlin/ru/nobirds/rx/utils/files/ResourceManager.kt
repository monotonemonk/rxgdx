package ru.nobirds.rx.utils.files

interface ResourceManager {

    fun forRead(name:String): ImmutableResource

    fun forWrite(name:String): MutableResource

    fun exists(name:String):Boolean

    fun sub(name:String): ResourceManager = SubResourceManager(this, name)

}

class ResourceNotFoundException(val name:String) : RuntimeException("Resource $name not found.")

