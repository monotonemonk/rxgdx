package ru.nobirds.rx.utils

interface ImmutableSize {

    val width:Float
    val height:Float

    companion object {
        fun readonly(width: Float, height: Float): ImmutableSize = ReadonlySize(width, height)
    }

}

interface MutableSize : ImmutableSize {

    override var width:Float
    override var height:Float

    fun size(width:Float, height:Float): MutableSize = apply {
        this.width = width
        this.height = height
    }

    fun size(size: ImmutableSize): MutableSize = size(size.width, size.height)

    fun copySize(): MutableSize = zero().size(this)

    companion object : PoolFactory<MutableSize> {

        override fun create(): MutableSize = zero()

        fun size(width: Float = 0f, height: Float = 0f): MutableSize = Size(width, height)
        fun zero(): MutableSize = size()
    }

}

fun ImmutableSize.plus(width: Float, height: Float):ImmutableSize = ImmutableSize.readonly(this.width + width, this.height + height)
operator fun ImmutableSize.plus(size:ImmutableSize):ImmutableSize = plus(size.width, size.height)

operator fun ImmutableSize.plus(spacing: ImmutableSpacing): ImmutableSize = plus(spacing.left + spacing.right, spacing.top + spacing.bottom)

fun MutableSize.max(width: Float, height: Float):MutableSize = size(Math.max(this.width, width), Math.max(this.height, height))
fun MutableSize.min(width: Float, height: Float):MutableSize = size(Math.min(this.width, width), Math.min(this.height, height))

fun MutableSize.concat(bounds: ImmutableBounds): MutableSize = max(bounds.x + bounds.width, bounds.y + bounds.height)

internal data class ReadonlySize(override val width: Float, override val height: Float) : ImmutableSize
internal data class Size(override var width: Float, override var height: Float) : MutableSize