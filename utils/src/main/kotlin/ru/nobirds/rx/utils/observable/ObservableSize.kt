package ru.nobirds.rx.utils.observable

import ru.nobirds.rx.observable.Observable
import ru.nobirds.rx.observable.Signal
import ru.nobirds.rx.property.Invalidatable
import ru.nobirds.rx.property.Property
import ru.nobirds.rx.property.changed
import ru.nobirds.rx.property.property
import ru.nobirds.rx.utils.MutableSize

class ObservableSize(x: Float = 0f, y: Float = 0f) : MutableSize, Invalidatable {

    val widthProperty: Property<Float> = property(x)
    override var width: Float by widthProperty

    val heightProperty: Property<Float> = property(y)
    override var height: Float by heightProperty

    override val invalidated: Observable<Signal> = changed(widthProperty, heightProperty)
}
