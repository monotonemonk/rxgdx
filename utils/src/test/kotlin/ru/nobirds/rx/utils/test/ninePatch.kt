package ru.nobirds.rx.utils.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Pixmap
import org.junit.Test
import ru.nobirds.rx.utils.ImmutableSize
import ru.nobirds.rx.utils.ninepatch.NinePatchFactory

class NinePatchTest {

    @Test
    fun test1() {
        testApplication {
            val ninePatch = NinePatchFactory(Pixmap(Gdx.files.classpath("nine-patch/test.9.png")))
                    .createNinePatch()

            val regions = ninePatch.scaleTo(ImmutableSize.readonly(123f, 75f))

            println(regions)
        }
    }
}